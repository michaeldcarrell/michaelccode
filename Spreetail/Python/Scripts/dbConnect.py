import pandas as pd
import pyodbc as odbc


def spreequery(server, database, query):
    conn = odbc.connect('Driver={SQL Server};'
                        'Server=' + server + ';'
                        'Database=' + database + ';'
                        'Trusted_Connection=yes')

    return pd.read_sql(query, conn)


print(spreequery('zoidberg-ro.lapkosoft.local', 'm2', 'SELECT * FROM dbo.amz_Fees'))
