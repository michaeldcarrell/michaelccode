import zipsToCall as Ztc
import CallProcess
import time
import datetime
import writeLog as Log
import getZips as GetZips

# while True
cycle_count = 0
fail_count = 0
while True:
    cycle_count += 1
    callzips = Ztc.zipstocall()

    callNumber = 0
    success_count = 0
    for zipCode in callzips:
        callNumber += 1
        call_fail = False
        time_stamp = str(datetime.datetime.now().date()) + ' ' + str(datetime.datetime.now().time())
        loop_info = 'Cycle Count: ' + str(cycle_count) + ', Call Number: ' + str(callNumber)
        # CallProcess.callloop(zipCode)
        try:
            call_results = CallProcess.callloop(zipCode)
            if call_results['call_status']:
                success_count += 1
            call_print_result = str(call_results['zip_data'])
        except:
            fail_count += 1
            call_print_result = 'API Call Failed Moving to Next Zip'
            call_fail = True
        collection_info = 'New Zipcodes Found: ' + str(success_count) + ', Total Failed Calls: ' + str(fail_count)
        Log.log_write(
            time_stamp +
            '\n' + loop_info +
            '\n' + GetZips.create_request(zipCode) +
            '\n' + call_print_result +
            '\n' + collection_info + '\n'
        )
        print(
            time_stamp +
            '\n' + loop_info +
            '\n' + GetZips.create_request(zipCode) +
            '\n' + call_print_result +
            '\n' + collection_info + '\n'
        )
        time.sleep(9)
