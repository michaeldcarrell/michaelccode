import requests


def create_request(zipcode):
    baseurl = "http://api.geonames.org/postalCodeSearchJSON?postalcode="
    username = "&username=mikace2005"
    country = "&country=US"
    postalcode = str(zipcode)
    finalurl = baseurl + postalcode + country + username
    return finalurl


def zipcall(finalurl):
    zipjson = requests.get(finalurl)
    return {'zip_call_data': zipjson.json(), 'api_request': finalurl}
