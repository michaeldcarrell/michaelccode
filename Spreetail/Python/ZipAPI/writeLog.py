def log_write(text):
    with open('log.txt', 'r+') as log_file:
        current_log = log_file.read()
        log_file.seek(0, 0)
        log_file.write(text + '\n' + current_log)
