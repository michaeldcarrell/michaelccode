import pyodbc
import pandas as pd

wwi = pyodbc.connect('Driver={SQL Server};'
                     'Server=.;'
                     'Database=Location;'
                     'Trusted_Connection=yes')


def query(sql):
    return pd.read_sql(sql, wwi)


def writepcc(zipcode):
    cursor = wwi.cursor()
    cursor.execute(
        f'''
        TRUNCATE TABLE etldata.PostCodeCall;
        INSERT INTO etldata.PostCodeCall (
            ZipCode
        )
        VALUES ('{zipcode}');

        EXEC etl.usp_Merge_PostCodeCall
        '''
    )
    cursor.commit()


def writezips(zipdata):
    if 'adminCode2' not in zipdata['postalCodes'][0]:
        admincode2 = 'NULL'
    else:
        admincode2 = f"'{zipdata['postalCodes'][0]['adminCode2']}'"

    if 'adminCode1' not in zipdata['postalCodes'][0]:
        admincode1 = 'NULL'
    else:
        admincode1 = f"'{zipdata['postalCodes'][0]['adminCode1']}'"

    if 'adminName2' not in zipdata['postalCodes'][0]:
        adminname2 = 'NULL'
    else:
        adminname2str = zipdata['postalCodes'][0]['adminName2'].replace("'", "''")
        adminname2 = f"'{adminname2str}'"

    if 'lng' not in zipdata['postalCodes'][0]:
        lng = 'NULL'
    else:
        lng = zipdata['postalCodes'][0]['lng']

    if 'countryCode' not in zipdata['postalCodes'][0]:
        countrycode = 'NULL'
    else:
        countrycode = f"'{zipdata['postalCodes'][0]['countryCode']}'"

    if 'postalCode' not in zipdata['postalCodes'][0]:
        postalcode = 'NULL'
    else:
        postalcode = f"'{zipdata['postalCodes'][0]['postalCode']}'"

    if 'adminName1' not in zipdata['postalCodes'][0]:
        adminname1 = 'NULL'
    else:
        adminname1 = f"'{zipdata['postalCodes'][0]['adminName1']}'"

    if 'ISO3166-2' not in zipdata['postalCodes'][0]:
        iso = 'NULL'
    else:
        iso = f"'{zipdata['postalCodes'][0]['ISO3166-2']}'"

    if 'placeName' not in zipdata['postalCodes'][0]:
        placename = 'NULL'
    else:
        placenamestr = zipdata['postalCodes'][0]['placeName'].replace("'", "''")
        placename = f"'{placenamestr}'"

    if 'lat' not in zipdata['postalCodes'][0]:
        lat = 'NULL'
    else:
        lat = zipdata['postalCodes'][0]['lat']

    cursor = wwi.cursor()
    cursor.execute(
        f'''
        TRUNCATE TABLE etldata.Zips;
        INSERT INTO etldata.Zips (
            rownames,
            adminCode2,
            adminCode1,
            adminName2,
            lng,
            countryCode,
            postalCode,
            adminName1,
            ISO31662,
            placeName,
            lat
        )
        VALUES ('1',
            {admincode2},
            {admincode1},
            {adminname2},
            {lng},
            {countrycode},
            {postalcode},
            {adminname1},
            {iso},
            {placename},
            {lat}
        );

        EXEC etl.usp_Merge_Zips
        '''
    )
    cursor.commit()
