def zipstocall():
    import numpy as np
    import dbConnect as db

    dbzips = db.query("SELECT * FROM dbo.Zips")
    dbcalls = db.query("SELECT * FROM dbo.PostCodeCall")

    usedzips = list(dbzips['PostalCode']) + list(dbcalls['ZipCode'])
    usedzips = list(set(usedzips))

    theoryzips = list(map(str, range(1, 100000)))
    counter = 0
    for zipcode in theoryzips:
        theoryzips[counter] = zipcode.rjust(5, '0')
        counter += 1

    callzips = np.setdiff1d(theoryzips, usedzips)

    if len(callzips) == 0:
        old_zips_call = db.query("""
            SELECT ZipCode, MAX(LastModifiedAt) MostRecentCall FROM PostCodeCall pcc
            LEFT OUTER JOIN Zips z ON pcc.ZipCode = z.PostalCode
            WHERE z.PostalCode IS NULL
            GROUP BY ZipCode
            ORDER BY MostRecentCall
        """)

        callzips = list(old_zips_call['ZipCode'])

    return callzips


print(zipstocall())
