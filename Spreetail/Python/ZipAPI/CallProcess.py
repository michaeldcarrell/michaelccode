import getZips
import dbConnect as db


def callloop(zipcode):
    call_success = False
    zipdata = getZips.zipcall(getZips.create_request(zipcode))
    if len(zipdata['zip_call_data']['postalCodes']) != 0:
        db.writezips(zipdata['zip_call_data'])
        call_success = True
    db.writepcc(zipcode)
    result_list = {'call_status': call_success,
                   'zip_data': zipdata['zip_call_data']['postalCodes']}

    return result_list
