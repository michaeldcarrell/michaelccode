import pyodbc
import pandas as pd


def server_connect(server_name, server_database):
    conn = pyodbc.connect(
        'Driver={SQL Server};'
        f'Server={server_name}.lapkosoft.local;'
        f'Database={server_database};'
        'Trusted_Connection=yes'
    )

    return conn


def sql_query(server, sql):
    return pd.read_sql(sql, server)


def sql_statement(server, sql):
    cursor = server.cursor()
    cursor.execute(sql)
    cursor.commit()


