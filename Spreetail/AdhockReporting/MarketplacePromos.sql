WITH WalmartDSV AS (
  SELECT SUM(sv.Value) WalmartDSVCost,
         sv.SaleID
  FROM m2.dbo.sales_Values AS sv
  WHERE sv.ValueTypeID = 132
  GROUP BY sv.SaleID
),
RMA AS (
  SELECT rma.SaleID,
         MAX(rma.RmaID) RMAs
  FROM m2.dbo.RMAs rma
  WHERE rma.SecondLevelResponseID = 54
  OR rma.SecondLevelResponseID = 55
  GROUP BY SaleID
),
Orders AS (
  SELECT bt.SaleID,
         bt.DateOfSale,
         bt.OrderID,
         bt.SourceOfSale,
         bt.QtySold,
         CASE
           WHEN
             CASE
                WHEN bt.SourceOfSale = 'WalmartDSV' THEN WalmartDSV.WalmartDSVCost
                ELSE bt.SalePrice
             END
           IS NULL THEN bt.SalePrice
           ELSE
             CASE
                WHEN bt.SourceOfSale = 'WalmartDSV' THEN WalmartDSV.WalmartDSVCost
                ELSE bt.SalePrice
             END
         END SalePrice,
         CASE
            WHEN bt.StatusID != 3 THEN 0
            WHEN RMA.RMAs IS NOT NULL THEN 0
            ELSE bt.AdjustmentFee
         END AdjustmentFee
  FROM btdata.dbo.Sales bt
  LEFT JOIN RMA ON RMA.SaleID = bt.SaleID
  LEFT JOIN WalmartDSV ON WalmartDSV.SaleID = bt.SaleID
  WHERE DateOfSale >= '1/1/2016'
),
Gross AS (
  SELECT MIN(DateOfSale) AS DATE,
         REPLACE(CONVERT(CHAR, OrderID), 'x', '') OrderID,
         SUM(QtySold*SalePrice)+SUM(AdjustmentFee) Gross
  FROM Orders
  GROUP BY REPLACE(CONVERT(CHAR, OrderID), 'x', '')
  HAVING MIN(CAST(DateOfSale AS DATE)) BETWEEN '1/1/2016' AND GETDATE()
),
OrderSales AS (
  SELECT REPLACE(CONVERT(CHAR, OrderID), 'x', '') OrderID,
         MIN(bt.SaleID) SaleID
  FROM btdata.dbo.Sales bt
  WHERE bt.DateOfSale >= '1/1/2016'
  GROUP BY REPLACE(CONVERT(CHAR, OrderID), 'x', '')
),
textFromSaleID AS (
  SELECT bt.SaleID,
         i.ItemID,
         bt.TypeOfPayment,
         bt.SourceOfSale,
         bt.QtySold,
         s.SupplierID
  FROM btdata.dbo.Sales bt
  INNER JOIN btdata.dbo.Listings l ON l.ListingID = bt.ListingID
  INNER JOIN btdata.dbo.Items i ON i.ItemID = l.ItemID
  INNER JOIN btdata.dbo.Inventory inv ON inv.InventoryID = i.InventoryID
  INNER JOIN btdata.dbo.Suppliers s ON s.SupplierID = inv.SupplierID
  WHERE bt.DateOfSale >= '1/1/2016'
),
ItemCostExtended AS (
  SELECT sv.SaleID,
         SUM(sv.Value) itemCostExtended
  FROM m2.dbo.sales_Values sv
  WHERE sv.ValueTypeID IN (103, 109)
  GROUP BY sv.SaleID
),
Dropship AS (
  SELECT SUM(sv.Value) DropshipFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 2
  GROUP BY sv.SaleID
),
Shipping AS (
  SELECT SUM(sv.Value) AS ShippingFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 1
  GROUP BY sv.SaleID
),
Storage AS (
  SELECT SUM(sv.Value) AS StorageFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 8
  GROUP BY sv.SaleID
),
Miscellaneous AS (
  SELECT SUM(sv.Value) AS MiscellaneousFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 3
  GROUP BY sv.SaleID
),
Marketplace AS (
  SELECT SUM(sv.Value) AS MarketplaceFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 9
  GROUP BY sv.SaleID
),
Amazon AS (
  SELECT SUM(sv.Value) AS AmazonFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 4
  GROUP BY sv.SaleID
),
Sears AS (
  SELECT SUM(sv.Value) AS SearsFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 10
  GROUP BY sv.SaleID
),
Jet AS (
  SELECT SUM(sv.Value) AS JetFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 11
  GROUP BY sv.SaleID
),
VM AS (
  SELECT SUM(sv.Value) AS VMFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 12
  GROUP BY sv.SaleID
),
Walmart AS (
  SELECT SUM(sv.Value) AS WalmartFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 5
  GROUP BY sv.SaleID
),
Wish AS (
  SELECT SUM(sv.Value) AS WishFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 13
  GROUP BY sv.SaleID
),
Newegg AS (
  SELECT SUM(sv.Value) AS NeweggFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 14
  GROUP BY sv.SaleID
),
GoogleExpress AS (
  SELECT SUM(sv.Value) AS GoogleExpressFee,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 15
  GROUP BY sv.SaleID
),
Fulfillment AS (
  SELECT SUM(sv.Value) AS FulfillmentCost,
         sv.SaleID
  FROM m2.dbo.sales_Values sv
  JOIN m2.dbo.sales_ValueTypes svt ON sv.ValueTypeID = svt.ValueTypeID
  WHERE svt.IsPrinicpal = 0
  AND svt.ValueTypeGroupID = 2
  GROUP BY sv.SaleID
),
SupplierSales AS (
  SELECT REPLACE(CONVERT(CHAR, bt.OrderID), 'x', '') OrderID,
         SUM(bt.ShippingCost) ShippingCost,
         SUM(bt.ListingFee) ListingFee,
         SUM(bt.FVF) FVF,
         SUM(bt.PayPalFees) PayPalFees,
         SUM(
           CASE
             WHEN (ItemCostExtended.itemCostExtended = 0 OR itemCostExtended.itemCostExtended IS NULL) THEN bt.ItemCost
             ELSE itemCostExtended.itemCostExtended
           END
          ) Cost,
         SUM(Dropship.DropshipFee) DropshipFee,
         SUM(Shipping.ShippingFee) ShippingFee,
         SUM(Storage.StorageFee) StorageFee,
         SUM(Miscellaneous.MiscellaneousFee) MiscellaneousFee,
         SUM(Marketplace.MarketplaceFee) MarketplaceFee,
         SUM(Amazon.AmazonFee) AmazonFee,
         SUM(Sears.SearsFee) SearsFee,
         SUM(VM.VMFee) VMFee,
         SUM(
          CASE
            WHEN bt.SourceOfSale = 'WalmartDSV' THEN 0
            ELSE Walmart.WalmartFee
          END
          ) WalmartFee,
         SUM(Wish.WishFee) WishFee,
         SUM(Newegg.NeweggFee) NeweggFee,
         SUM(JetFee) JetFee,
         SUM(GoogleExpress.GoogleExpressFee) GoogleExpressFee,
         SUM(Fulfillment.FulfillmentCost) FulfillmentCost
  FROM btdata.dbo.Sales bt
  LEFT JOIN ItemCostExtended ON bt.SaleID = ItemCostExtended.SaleID
  LEFT JOIN Dropship ON bt.SaleID = Dropship.SaleID
  LEFT JOIN Shipping ON bt.SaleID = Shipping.SaleID
  LEFT JOIN Storage ON bt.SaleID = Storage.SaleID
  LEFT JOIN Miscellaneous ON bt.SaleID = Miscellaneous.SaleID
  LEFT JOIN Marketplace ON bt.SaleID = Marketplace.SaleID
  LEFT JOIN Amazon ON bt.SaleID = Amazon.SaleID
  LEFT JOIN Sears ON bt.SaleID = Sears.SaleID
  LEFT JOIN Jet ON bt.SaleID = Jet.SaleID
  LEFT JOIN VM ON bt.SaleID = VM.SaleID
  LEFT JOIN Walmart ON bt.SaleID = Walmart.SaleID
  LEFT JOIN Wish ON bt.SaleID = Wish.SaleID
  LEFT JOIN Newegg ON bt.SaleID = Newegg.SaleID
  LEFT JOIN GoogleExpress ON bt.SaleID = GoogleExpress.SaleID
  LEFT JOIN Fulfillment ON bt.SaleID = Fulfillment.SaleID
  WHERE bt.DateOfSale >='1/1/2016'
  GROUP BY REPLACE(CONVERT(CHAR, bt.OrderID), 'x', '')
),
S1 AS (
  SELECT Gross.DATE SALESDATE,
         Gross.OrderID,
         ROUND(Gross.Gross, 2) GMV,
         textFromSaleID.SaleID,
         textFromSaleID.ItemID,
         textFromSaleID.TypeOfPayment,
         textFromSaleID.SourceOfSale,
         textFromSaleID.QtySold,
         textFromSaleID.SupplierID,
         SupplierSales.ShippingCost,
         ROUND(COALESCE(SupplierSales.ListingFee, 0) +
               COALESCE(SupplierSales.FVF, 0) +
               COALESCE(SupplierSales.AmazonFee, 0) +
               COALESCE(SupplierSales.SearsFee, 0) +
               COALESCE(SupplierSales.JetFee, 0) +
               COALESCE(SupplierSales.VMFee, 0) +
               COALESCE(SupplierSales.WalmartFee, 0) +
               COALESCE(SupplierSales.WishFee, 0) +
               COALESCE(SupplierSales.NeweggFee, 0) +
               COALESCE(SupplierSales.GoogleExpressFee, 0) +
               COALESCE(SupplierSales.PayPalFees, 0) +
               COALESCE(SupplierSales.MiscellaneousFee, 0) +
               COALESCE(SupplierSales.MarketplaceFee, 0)
         , 2) MARKETPLACEFEES,
         CASE
            WHEN textFromSaleID.SupplierID = 137 THEN 0
            ELSE SupplierSales.Cost
         END CORECOGS,
         SupplierSales.DropshipFee,
         SupplierSales.ShippingFee,
         SupplierSales.StorageFee,
         SupplierSales.FulfillmentCost,
         ROUND(COALESCE(SupplierSales.ShippingFee, 0)+
               COALESCE(SupplierSales.StorageFee, 0) +
               COALESCE(SupplierSales.FulfillmentCost, 0) +
               COALESCE(SupplierSales.ShippingCost, 0)
         , 2) NONMARKETPLACEFEES,
         COALESCE(SupplierSales.StorageFee, 0) + COALESCE(SupplierSales.FulfillmentCost, 0) Operations_$,
         CASE
            WHEN textFromSaleID.TypeOfPayment = 'GOOGLE CHECKOUT' THEN 'GOOGLEEXPRESS'
            ELSE textFromSaleID.TypeOfPayment
         END SFP_MARKETPLACE
  FROM Gross
  LEFT JOIN OrderSales ON Gross.OrderID = OrderSales.OrderID
  LEFT JOIN textFromSaleID ON OrderSales.SaleID = textFromSaleID.SaleID
  LEFT JOIN SupplierSales ON Gross.OrderID = SupplierSales.OrderID
  WHERE ROUND(Gross.Gross, 2) > 0
),
Rebate AS (
  SELECT vr.Supplier,
         vr.[Supplier ID] SupplierID,
         vr.[Start Date] StartDate,
         vr.[Projected End Date] EndDate,
         vr.[% of FIFO] PctFIFO
  FROM BusinessAnalytics.dbo.VendorRebates vr
),
S2 AS (
  SELECT S1.SaleID,
         S1.SALESDATE,
         SUM(CONVERT(FLOAT, REPLACE(CONVERT(CHAR, Rebate.PctFIFO), '%', '')))/100 REBATE
  FROM S1
  LEFT JOIN Rebate ON S1.SupplierID = Rebate.SupplierID
    AND S1.SALESDATE>=Rebate.StartDate
    AND S1.SALESDATE<=Rebate.EndDate
  GROUP BY S1.SaleID, S1.SALESDATE
),
S3 AS (
  SELECT S1.*,
         S2.REBATE [Rebate_%],
         ROUND(COALESCE(S1.CORECOGS,0)*COALESCE(S2.REBATE,0), 2) REBATE_$
  FROM S1
  LEFT JOIN S2 ON S1.SaleID = S2.SaleID
),
BA_Marketplaces AS (
  SELECT * FROM BusinessAnalytics.dbo.Marketplace
),
SupplierSalesGross AS (
  SELECT S3.SALESDATE,
         S3.OrderID,
         S3.GMV,
         S3.SaleID,
         S3.ItemID,
         S3.TypeOfPayment,
         S3.SourceOfSale,
         S3.SFP_MARKETPLACE,
         BA_Marketplaces.[Report Marketplace] REPORT_MARKETPLACE,
         S3.QtySold,
         S3.ShippingCost,
         S3.MARKETPLACEFEES,
         S3.NONMARKETPLACEFEES,
         S3.CORECOGS,
         S3.DropshipFee,
         S3.ShippingFee,
         S3.StorageFee,
         S3.FulfillmentCost,
         S3.Operations_$,
         S3.[Rebate_%],
         S3.REBATE_$,
         ROUND(COALESCE(S3.CORECOGS, 0)-COALESCE(S3.REBATE_$, 0), 2) COGS,
         ROUND(COALESCE(S3.GMV, 0)-COALESCE(S3.CORECOGS, 0)-S3.MARKETPLACEFEES-S3.NONMARKETPLACEFEES, 2) CORE_GMV_CM_$,
         ROUND(COALESCE(S3.GMV, 0)-COALESCE(S3.CORECOGS, 0)-COALESCE(S3.REBATE_$, 0)-S3.MARKETPLACEFEES-S3.NONMARKETPLACEFEES, 2) GMV_CM_$,
         CASE
            WHEN S3.SupplierID=137 THEN CONCAT(BA_Marketplaces.[Report Marketplace],' - ', 'Resell')
            ELSE BA_Marketplaces.[Report Marketplace]
         END RESELL_MARKETPLACE
  FROM S3
  LEFT JOIN BA_Marketplaces ON S3.SFP_MARKETPLACE = BA_Marketplaces.[SFP Marketplace]
),
Aggregate AS (
  SELECT ssg.ItemID,
  ssg.SOURCEOFSALE,
  SUM(ssg.QTYSOLD) QtyLast7,
  SUM(ssg.GMV) GmvLast7,
  SUM(ssg.GMV_CM_$) GmvCmLast7,
  ROW_NUMBER() OVER(PARTITION BY ITEMID ORDER BY SUM(ssg.GMV) DESC) ROW_NUM --Ranks Marketplaces by GMV inside ITEMID
  FROM SupplierSalesGross ssg
  WHERE SALESDATE >= DATEADD(DAY, -7, GETDATE()) --Apply 7 day range from current date
  GROUP BY ITEMID, SOURCEOFSALE
),
NewestSalePrice AS (
  SELECT np.ITEMID, np.GMV/np.QTYSOLD LatestUnitPrice, np.SOURCEOFSALE
  FROM SupplierSalesGross np
  JOIN (
    SELECT MAX(SALEID) MaxSaleID, ITEMID, SOURCEOFSALE
    FROM SupplierSalesGross
    GROUP BY ITEMID, SOURCEOFSALE
  ) msid ON msid.MaxSaleID = np.SALEID AND msid.SOURCEOFSALE = np.SOURCEOFSALE
  WHERE np.QTYSOLD != 0
),
QtyOnHand AS (
  SELECT ITEMID, SUM(ADJUSTMENTQTY) TotalOnHand
  FROM m2.dbo.LSInventory
  GROUP BY ITEMID
  HAVING SUM(ADJUSTMENTQTY) != 0
)

-----------------------------------------------------------------------------------------------------------------------

SELECT agg.ItemID,
       inv.PartNum,
       agg.QtyLast7,
       agg.GmvCmLast7,
       agg.GmvCmLast7,
       (agg.GmvCmLast7/agg.GmvLast7) CmPercentLast7,
       CASE
          WHEN ist.StatusID NOT IN (14, 22) AND qty.TotalOnHand > 0 THEN (agg.QtyLast7/qty.TotalOnHand)
          ELSE NULL
       END PercQONtoSalesDom,
       CASE
          WHEN ist.StatusID IN (14, 22) AND qty.TotalOnHand > 0 THEN (agg.QtyLast7/qty.TotalOnHand)
          ELSE NULL
       END PercQONtoSalesInt,
       CASE
          WHEN ist.StatusID NOT IN (14, 22)
              AND qty.TotalOnHand > 0
              AND (agg.QtyLast7/qty.TotalOnHand) < 0.1
          THEN 1
          WHEN ist.StatusID IN (14, 22)
              AND qty.TotalOnHand > 0
              AND (agg.QtyLast7/qty.TotalOnHand) < 0.03
          THEN 1
          ELSE 0
       END,
       nsp.LatestUnitPrice,
       qty.TotalOnHand,
       agg.SourceOfSale,
       CONCAT(u.FirstName, ' ', u.LastName) PrimaryAccountManager,
       gd.[Category Level 2],
       gd.[Category Level 4]
FROM Aggregate agg
JOIN btdata.dbo.Items item ON item.ItemID = agg.ItemID
JOIN btdata.dbo.Inventory inv ON inv.InventoryID = item.InventoryID
JOIN m2.dbo.items_ExtendedProperties ixp ON ixp.ItemID = item.ItemID
JOIN BusinessAnalytics.dbo.Google_Data gd On ixp.GoogleExpressCategoryID = gd.[Category ID]
JOIN m2.dbo.items_InventoryStatusType ist ON ist.StatusID = ixp.Closeout
JOIN QtyOnHand qty ON qty.ItemID = agg.ItemID
JOIN NewestSalePrice nsp ON nsp.ItemID = agg.ItemID AND nsp.SourceOfSale = agg.SourceOfSale
JOIN btdata.dbo.Suppliers s ON inv.SupplierID = s.SupplierID
JOIN m2.dbo.Users u on u.UserID = s.PrimaryAccountManager