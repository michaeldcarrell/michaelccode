import pandas as pd

zcta_to_place_url = 'http://www2.census.gov/geo/docs/maps-data/data/rel/zcta_place_rel_10.txt'

# load relevant data
df = pd.read_csv(zcta_to_place_url)

# the data often repeats the same (ZCTA, state) pair. Remove these
df = df.drop_duplicates()

# the census uses numeric state codes
# replace these with state names

census_codes_to_names_url = 'http://www2.census.gov/geo/docs/reference/state.txt'

states = pd.read_csv(census_codes_to_names_url, sep='|')
merged = pd.merge(df, states, on='STATE')
print(merged['GEOID'])
