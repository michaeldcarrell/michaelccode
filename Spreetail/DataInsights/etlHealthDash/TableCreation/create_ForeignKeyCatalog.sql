DROP TABLE IF EXISTS etl.ForeignKeyCatalog;
CREATE TABLE etl.ForeignKeyCatalog (
    KeyID INT IDENTITY(1,1) PRIMARY KEY,
    PrimaryKeyID INT NOT NULL,
    ForeignKeySourceTableID INT NULL, --Allow nulls to populate in order to allow for PK-FK combos where FK table is not in SpreeTL yet
    ForeignKeySourceDatabase NVARCHAR(128) NOT NULL,
    ForeignKeySourceSchema NVARCHAR(128) NOT NULL,
    ForeignKeySourceTable NVARCHAR(128) NOT NULL,
    ForeignKeySourceColumn NVARCHAR(128) NOT NULL,
    SourcePhysicalPrimaryKey BIT NOT NULL,
    ModifiedOn DATETIME DEFAULT GETDATE(),
    ModifiedBy NVARCHAR(128) DEFAULT SUSER_NAME(),
    CreatedAt DATETIME DEFAULT GETDATE(),
    CreatedBy NVARCHAR(128) DEFAULT SUSER_NAME(),
    FOREIGN KEY (PrimaryKeyID) REFERENCES etl.PrimaryKeyCatalog(PrimaryKeyID),
    FOREIGN KEY (ForeignKeySourceTableID) REFERENCES etl.SourceTable(SourceTableID)
)