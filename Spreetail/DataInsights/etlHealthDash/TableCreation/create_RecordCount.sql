DROP TABLE IF EXISTS etl.RecordSnapshot;
CREATE TABLE etl.RecordSnapshot (
    RecordCountID INT IDENTITY(1,1) PRIMARY KEY,
    TableName NVARCHAR(255),
    InsertedRecords INT,
    UpdatedRecords INT,
    TotalRecords INT,
    CreatedAt DATETIME DEFAULT GETDATE(),
    CreatedBy NVARCHAR(255) DEFAULT SUSER_NAME()
)