SELECT
    execution_id,
    folder_name,
    project_name,
    package_name,
    executed_as_name,
    start_time
FROM catalog.executions
ORDER BY start_time DESC

SELECT DISTINCT
    ex.execution_id,
    folder_name,
    project_name,
    ex.package_name,
    exables.package_name
FROM catalog.executions ex
LEFT OUTER JOIN catalog.executables exables ON ex.execution_id = exables.execution_id
ORDER BY ex.execution_id DESC