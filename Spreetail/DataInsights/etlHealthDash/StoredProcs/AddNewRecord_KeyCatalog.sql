CREATE OR ALTER PROCEDURE etl.AddKeyCombo
    @PK_DatabaseName NVARCHAR(128),
    @PK_SchemaName NVARCHAR(128),
    @PK_TableName NVARCHAR(128),
    @PK_ColumnName NVARCHAR(128),
    @FK_DatabaseName NVARCHAR(128),
    @FK_SchemaName NVARCHAR(128),
    @FK_TableName NVARCHAR(128),
    @FK_ColumnName NVARCHAR(128),
    @PhysicalKey BIT

AS

BEGIN
    --Only insert new keys if the pairing does not yet exist
    IF NOT EXISTS(
        SELECT
            KeyID
        FROM etl.ForeignKeyCatalog fkc
        LEFT OUTER JOIN etl.PrimaryKeyCatalog pkc ON pkc.PrimaryKeyID = fkc.PrimaryKeyID
        LEFT OUTER JOIN etl.SourceTable st ON st.SourceTableID = pkc.PrimaryKeyTableID
        WHERE st.SourceDatabase= @PK_DatabaseName
          AND st.SourceSchema = @PK_SchemaName
          AND st.SourceTableName = @PK_TableName
          AND pkc.PrimaryKeyColumnName = @PK_ColumnName
          AND ForeignKeySourceDatabase = @FK_DatabaseName
          AND ForeignKeySourceSchema = @FK_SchemaName
          AND ForeignKeySourceTable = @FK_TableName
          AND ForeignKeySourceColumn = @FK_ColumnName
    )
        BEGIN

            DECLARE @FK_SourceTableID AS INT

            --Look for FK TableID in SourceTables
            IF EXISTS(SELECT
                    SourceTableID
                FROM etl.SourceTable
                WHERE SourceDatabase = @FK_DatabaseName
                  AND SourceSchema = @FK_SchemaName
                  AND SourceTableName = @FK_TableName
                )
                BEGIN
                    SET @FK_SourceTableID = (
                        SELECT
                            SourceTableID
                        FROM etl.SourceTable
                        WHERE SourceDatabase = @FK_DatabaseName
                          AND SourceSchema = @FK_SchemaName
                          AND SourceTableName = @FK_TableName
                    )
                END
            ELSE
                BEGIN
                    SET @FK_SourceTableID = NULL
                END

            --Insert Into etl.KeyCatalog
            INSERT INTO etl.ForeignKeyCatalog(
                PrimaryKeyID,
                ForeignKeySourceTableID,
                ForeignKeySourceDatabase,
                ForeignKeySourceSchema,
                ForeignKeySourceTable,
                ForeignKeySourceColumn,
                SourcePhysicalPrimaryKey
            )
            VALUES(
                  (SELECT TOP 1 SourceTableID
                   FROM etl.SourceTable st
                   LEFT OUTER JOIN etl.PrimaryKeyCatalog pk ON pk.PrimaryKeyTableID = st.SourceTableID
                   WHERE SourceDatabase = @PK_DatabaseName
                   AND SourceSchema = @PK_SchemaName
                   AND SourceTableName = @PK_TableName
                   AND PrimaryKeyColumnName = @PK_ColumnName
                   ),
                   @FK_SourceTableID,
                   @FK_DatabaseName,
                   @FK_SchemaName,
                   @FK_TableName,
                   @FK_ColumnName,
                   @PhysicalKey
            )
        END
END