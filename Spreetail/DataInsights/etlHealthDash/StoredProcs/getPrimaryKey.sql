CREATE OR ALTER PROCEDURE etl.GetPrimaryKeys

AS

BEGIN

    DECLARE @tableName AS NVARCHAR(128),
        @procnames CURSOR,
        @currentProcName AS NVARCHAR(255)

    SET @procnames = CURSOR FOR (
        SELECT CONCAT(ROUTINE_SCHEMA, '.', ROUTINE_NAME) ProcName
        FROM INFORMATION_SCHEMA.ROUTINES procs
        WHERE procs.SPECIFIC_SCHEMA = 'ssis'
        AND SPECIFIC_NAME LIKE 'usp_Merge_stage_%'
    )



    OPEN @procnames
    FETCH NEXT FROM @procnames INTO @currentProcName
    WHILE (@@fetch_status = 0)
    BEGIN
        DROP TABLE IF EXISTS #ProcText;
        CREATE TABLE #ProcText (
            Text NVARCHAR(1000)
        );

        SET @tableName = (SELECT REPLACE(@currentProcName, 'ssis.usp_Merge_stage_', ''))

        INSERT INTO #ProcText (Text)
        EXEC sp_helptext @currentProcName

        INSERT INTO etl.KeyCatalog(KeyTable, KeyName)
        SELECT TableName, PrimaryKey
        FROM (
             SELECT TOP 1
                @tableName TableName,
                SUBSTRING([Text], CHARINDEX('.', [Text]) + 1, CHARINDEX('=', [Text]) - (CHARINDEX('.', [Text]) + 1)) PrimaryKey
            FROM #ProcText
            WHERE [Text] LIKE '%etldata.%'
              AND [Text] LIKE '%=stage.%'
         ) GetPK
        LEFT OUTER JOIN etl.KeyCatalog kc ON GetPK.TableName = kc.KeyTable AND GetPK.PrimaryKey = kc.KeyName
        WHERE kc.KeyID IS NULL

        FETCH NEXT FROM @procnames INTO @currentProcName
    END
END