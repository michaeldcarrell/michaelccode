CREATE OR ALTER PROCEDURE etl.AddPrimaryKey
    @SourceTableName NVARCHAR(128),
    @SourceDatabaseName NVARCHAR(128),
    @SourceSchemaName NVARCHAR(128),
    @SourceTablePrimaryKey NVARCHAR(128),
    @ReplaceKey BIT = 0 --Default replacing key to false to prevent overwriting keys unless intended

AS

BEGIN
    DECLARE @SourceTableID INT

    --If table is in SourceTable
    IF EXISTS(
            SELECT
                SourceTableID
            FROM etl.SourceTable
            WHERE SourceTableName = @SourceTableName
            AND SourceSchema = @SourceSchemaName
            AND SourceDatabase = @SourceDatabaseName
        )
        --If the key doesn't already exist
        IF NOT EXISTS(
            SELECT PrimaryKeyID
            FROM etl.PrimaryKeyCatalog pkc
            LEFT OUTER JOIN etl.SourceTable st ON st.SourceTableID = pkc.PrimaryKeyTableID
            WHERE PrimaryKeyColumnName = @SourceTablePrimaryKey
            AND SourceTableName = @SourceTableName
            AND SourceSchema = @SourceSchemaName
            AND SourceDatabase = @SourceDatabaseName
            )
            --Create the key
            BEGIN
                SET @SourceTableID = (
                    SELECT TOP 1 SourceTableID FROM etl.SourceTable
                    WHERE SourceTableName = @SourceTableName
                    AND SourceSchema = @SourceSchemaName
                    AND SourceDatabase = @SourceDatabaseName
                )

                INSERT INTO etl.PrimaryKeyCatalog (PrimaryKeyTableID, PrimaryKeyColumnName)
                VALUES(@SourceTableID, @SourceTablePrimaryKey)

                SELECT CONCAT('Inserted Primary Key for ', @SourceDatabaseName, '.', @SourceSchemaName, '.', @SourceTableName, ' as ', @SourceTablePrimaryKey) Result
            END
        --If the key already exists
        ELSE
            --If replacement was specified
            IF @ReplaceKey = 1
                --Update primary key
                BEGIN
                    UPDATE etl.PrimaryKeyCatalog
                    SET PrimaryKeyColumnName = @SourceTablePrimaryKey,
                        ModifiedOn = GETDATE(),
                        ModifiedBy = SUSER_NAME()
                    WHERE PrimaryKeyTableID = (
                        SELECT SourceTableID
                        FROM etl.SourceTable
                        WHERE SourceTableName = @SourceTableName
                        AND SourceDatabase = @SourceDatabaseName
                        AND SourceSchema =  @SourceSchemaName
                    )

                    SELECT CONCAT('Primary Key for ', @SourceDatabaseName, '.', @SourceSchemaName, '.', @SourceTableName, ' updated to ', @SourceTablePrimaryKey) Result
                END
            --Inform that key is already created
            ELSE
                SELECT CONCAT(@SourceTablePrimaryKey, ' is already set as the Primary Key for ', @SourceDatabaseName, '.', @SourceSchemaName, '.', @SourceTableName) Result
    --Inform that they source table does not yet exist
    ELSE
        SELECT 'etl.SourceTable record does not exist for the table specified' Result

END