CREATE OR ALTER PROCEDURE etl.GetRecordCount

AS

BEGIN
    DECLARE @currentTable CURSOR,
        @tableName NVARCHAR(255)

    SET @currentTable = CURSOR FOR (
        SELECT
            TABLE_NAME
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE COLUMN_NAME = 'ChangeOperation'
          AND TABLE_SCHEMA = 'etldata'
    )

    OPEN @currentTable
    FETCH NEXT FROM @currentTable INTO @tableName
    WHILE (@@fetch_status = 0)
    BEGIN

        DECLARE @sql NVARCHAR(500)
        SET @sql = 'SELECT ''' + @tableName + ''' TableName, InsertCount, UpdateCount, TotalCount
                    FROM (SELECT COUNT(*) InsertCount FROM etldata.' + @tableName +' WHERE ChangeOperation = ''I'') ins
                    JOIN (SELECT COUNT(*) UpdateCount FROM etldata.' + @tableName +' WHERE ChangeOperation = ''U'') up ON 1=1
                    JOIN (SELECT COUNT(*) TotalCount FROM stage.' + @tableName + ') tr ON 1=1'

        INSERT INTO etl.RecordSnapshot(TableName, InsertedRecords, UpdatedRecords, TotalRecords)
        EXEC (@sql)

        FETCH NEXT FROM @currentTable INTO @tableName
    END
END