CREATE OR ALTER PROCEDURE etl.ChangeTableStatus
    @TableName NVARCHAR(128) = NULL,
    @DesiredStatus BIT = 1

AS

BEGIN

    DECLARE @StatusText NVARCHAR(100)

    IF @DesiredStatus = 0
        SET @StatusText = CONCAT(@TableName, ' was set to ', @DesiredStatus, ' (Inactive)')
    ELSE IF EXISTS(
        SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = @TableName
        )
        SET @StatusText = CONCAT(@TableName, ' was set to ', @DesiredStatus, ' (Active)')
    ELSE
        SET @StatusText = 'Table could not be set to active because it does not exist'


    IF @TableName IS NOT NULL
        UPDATE etl.SourceTable
        SET IsActive = @DesiredStatus
        WHERE SourceTableName = @TableName

        SELECT @StatusText Result

    --Inactivates any tables which are no longer in stage
    UPDATE etl.SourceTable
    SET IsActive = 0
    WHERE SourceTableName IN (
        SELECT
            SourceTableName
        FROM etl.SourceTable st
                 LEFT OUTER JOIN (
            SELECT
                TABLE_NAME
            FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_SCHEMA = 'stage'
        ) infotables ON st.SourceTableName = infotables.TABLE_NAME
        WHERE infotables.TABLE_NAME IS NULL
          AND st.IsActive = 1
    )

END