CREATE OR ALTER PROCEDURE ssis.usp_UpdateSourceTableProcessExecutionRecordCount
	@SourceTableProcessExecutionID BIGINT
	,@CompletedSuccessfully BIT
	,@ChangeTrackingCurrentVersionForNext BIGINT
	,@ChangeTrackingCurrentVersionForCurrentLoad BIGINT
	,@TableName NVARCHAR(100)

AS
/*
This procedure updates the process execution log with an end time, change tracking version for the next run,
	and whether it completed successfully.

If the execution of the table process was successful, the source table is updated with the next change tracking version.
*/

BEGIN

	DECLARE @SourceTableID INT
	SELECT @SourceTableID=SourceTableID
	FROM etl.SourceTableProcessExecution
	WHERE SourceTableProcessExecutionID=@SourceTableProcessExecutionID

	UPDATE etl.SourceTableProcessExecution
	SET ChangeTrackingCurrentVersionForNext=@ChangeTrackingCurrentVersionForNext
		,ProcessEnd=GETDATE()
		,CompletedSuccessfully=@CompletedSuccessfully
		,ModifiedOn=GETDATE()
		,ModifiedBy=SYSTEM_USER
	WHERE SourceTableProcessExecutionID=@SourceTableProcessExecutionID

	IF @CompletedSuccessfully=1
	BEGIN
		UPDATE etl.SourceTable
		SET ChangeTrackingCurrentVersionForNext=@ChangeTrackingCurrentVersionForNext
			,ModifiedOn=GETDATE()
			,ModifiedBy=SYSTEM_USER
		WHERE SourceTableID=@SourceTableID
	END

    /*Grab Record Counts from etldata table and insert into RecordCounts

      When the table has a stage and etldata tables which match the ssis package destination table name
      inserts counts for number of records for inserted records and update records from etldata table and
      inserts the total value count for table
     */
	--Test for exception tables
	DECLARE @Exception BIT
	DECLARE @sql NVARCHAR(2000) --Initialize @sql outside the conditional

	SET @Exception = (
        SELECT
            CAST(CASE
                     WHEN COUNT(DISTINCT TABLE_SCHEMA) = 2
                         THEN 0
                     ELSE 1
                END AS BIT) Exception
        FROM INFORMATION_SCHEMA.TABLES
        WHERE TABLE_SCHEMA IN ('etldata', 'stage')
          AND TABLE_NAME = @TableName
    )
    --Update Records into SourceTableProcessExecution
	IF @Exception = 0

        SET @sql = 'UPDATE etl.SourceTableProcessExecution
                           SET ETLInsertedRecords = rc.InsertCount
                              , ETLUpdatedRecords = rc.UpdateCount
                              , CurrentTotalRecords = rc.TotalCount
                           FROM etl.SourceTableProcessExecution stpe
                           LEFT OUTER JOIN (
                               SELECT ''' + @TableName + ''' TableName,
                               InsertCount,
                               UpdateCount,
                               TotalCount,
                               CAST(' + CAST(@SourceTableProcessExecutionID AS NVARCHAR(20)) + ' AS BIGINT) SourceTableProcessExecutionID
                                FROM (SELECT COUNT(*) InsertCount FROM etldata.' + @TableName +' WHERE ChangeOperation = ''I'') ins
                                JOIN (SELECT COUNT(*) UpdateCount FROM etldata.' + @TableName +' WHERE ChangeOperation = ''U'') up ON 1=1
                                JOIN (SELECT COUNT(*) TotalCount FROM stage.' + @TableName + ') tr ON 1=1
                           ) rc ON rc.SourceTableProcessExecutionID = stpe.SourceTableProcessExecutionID
                           WHERE rc.SourceTableProcessExecutionID IS NOT NULL'

        EXEC(@sql)
END