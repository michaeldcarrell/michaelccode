EXEC etl.AddKeyCombo @PK_DatabaseName = 'PrimaryKeyDatabase',
    @PK_SchemaName = 'PrimaryKeySchema',
    @PK_TableName = 'PrimaryKeyTable',
    @FK_DatabaseName = 'ForeignKeyDatabase',
    @FK_SchemaName = 'ForeignKeySchema',
    @FK_TableName = 'ForeignKeyTable',
    @FK_ColumnName = 'ForeignKeyColumn',
    --documents if they key is physically linked in source database (1 for keys that are, 0 for those not)
    @PhysicalKey = 0

EXEC etl.AddKeyCombo
    @PK_DatabaseName = 'm2',
    @PK_SchemaName = 'dbo',
    @PK_TableName = 'dp_PrintQueue',
    @FK_DatabaseName = 'm2',
    @FK_SchemaName = 'dbo',
    @FK_TableName = 'Printers',
    @FK_ColumnName = 'PrinterID',
    @PhysicalKey = 0

SELECT SourceDatabase PK_DB,
    SourceSchema PK_SCHEMA,
    SourceTableName PK_TABLE,
    ForeignKeySourceDatabase FK_DB,
    ForeignKeySourceSchema FK_SCHEMA,
    ForeignKeySourceTable FK_TABLE,
    ForeignKeySourceColumn FK_COLUMN
FROM etl.KeyCatalog kc
LEFT OUTER JOIN etl.SourceTable st ON st.SourceTableID = kc.PrimarySourceTableID
WHERE ForeignKeySourceDatabase = 'm2'
AND ForeignKeySourceSchema = 'dbo'
AND ForeignKeySourceTable = 'dp_PackageItems'