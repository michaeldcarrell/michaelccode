CREATE PROCEDURE [ssis].[usp_Merge_stage_NewTable]
							@SourceTableProcessExecutionID BIGINT
AS

BEGIN

		SET NOCOUNT ON

	INSERT INTO stage.NewTable (
		PrimaryKey
		--End Table Columns
		, IsIncrementalInsertOverlap
		, IsSourceSystemActive
		, SourceTableProcessExecutionID
	)

	SELECT etldata.PrimaryKey
		 --End Table Columns
			, CASE
					WHEN etldata.ChangeOperation = 'I' AND stage.PrimaryKey IS NOT NULL THEN 1
					ELSE 0
				END --IsIncrementalInsertOverlap
			, 1 --IsSourceSystemActive
			, @SourceTableProcessExecutionID
	FROM etldata.NewTable etldata
		LEFT OUTER JOIN stage.NewTable stage
			ON (etldata.PrimaryKey=stage.PrimaryKey)
	WHERE etldata.ChangeOperation = 'I'

	UPDATE stage
	SET
	--OtherColumn = etldata.OtherColumn
	--End Table Columns
		, SourceTableProcessExecutionID = @SourceTableProcessExecutionID
	FROM stage.NewTable stage
	INNER JOIN etldata.NewTable etldata ON (stage.PrimaryKey = etldata.PrimaryKey)
		AND (etldata.ChangeOperation = 'U')

  UPDATE stage
	SET IsSourceSystemActive=0
  	, SourceTableProcessExecutionID = @SourceTableProcessExecutionID
  FROM stage.NewTable stage
  INNER JOIN etldata.NewTable etldata ON (stage.PrimaryKey = etldata.PrimaryKey)
  	AND (etldata.ChangeOperation = 'D')

  SET NOCOUNT OFF

END


