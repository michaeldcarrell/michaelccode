CREATE TABLE etldata.Items (

  ChangeOperation NCHAR(1),
	SourceTableProcessExecutionID BIGINT
);

CREATE TABLE stage.Items (

  IsIncrementalInsertOverlap BIT,
	IsSourceSystemActive BIT,
	SourceTableProcessExecutionID BIGINT
);