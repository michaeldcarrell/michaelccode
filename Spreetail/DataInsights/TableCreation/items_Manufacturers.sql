CREATE TABLE etldata.items_Manufacturers (
  ManufID INT,
  ParentManufID INT,
  Name NVARCHAR(255),
  ChangeOperation NCHAR(1),
	SourceTableProcessExecutionID BIGINT
);

CREATE TABLE stage.items_Manufacturers (
  ManufID INT,
  ParentManufID INT,
  Name NVARCHAR(255),
  IsIncrementalInsertOverlap BIT,
	IsSourceSystemActive BIT,
	SourceTableProcessExecutionID BIGINT
);