DROP TABLE IF EXISTS dim.WarehouseLocation;
CREATE TABLE dim.WarehouseLocation
(
    DimWarehouseLocationID INT IDENTITY (1, 1) PRIMARY KEY,
    LocationID             INT          NOT NULL,
    WarehouseName          NVARCHAR(50) NOT NULL,
    WarehouseCity          NVARCHAR(50) NOT NULL,
    WarehouseState         NVARCHAR(50) NOT NULL,
    LocationName           NVARCHAR(50) NOT NULL,
    IsPackedProduct        NVARCHAR(30) NOT NULL,
    IsPickAndPack          NVARCHAR(30) NOT NULL,
    IsDropshipLocation     NVARCHAR(30) NOT NULL,
    CreatedAt DATETIME DEFAULT GETDATE(),
    CreatedBy NVARCHAR(128) DEFAULT SUSER_NAME()
)

SET IDENTITY_INSERT dim.WarehouseLocation ON;
INSERT INTO dim.WarehouseLocation (
    DimWarehouseLocationID,
    LocationID,
    WarehouseName,
    WarehouseCity,
    WarehouseState,
    LocationName,
    IsPackedProduct,
    IsPickAndPack,
    IsDropshipLocation
)
VALUES(0, 0, '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown')
SET IDENTITY_INSERT dim.WarehouseLocation OFF;

DROP TABLE IF EXISTS etl.WarehouseLocation;
CREATE TABLE etl.WarehouseLocation
(
    DimWarehouseLocationID INT,
    LocationID             INT,
    WarehouseName          NVARCHAR(50),
    WarehouseCity          NVARCHAR(50),
    WarehouseState         NVARCHAR(50),
    LocationName           NVARCHAR(50),
    IsPackedProduct        NVARCHAR(30),
    IsPickAndPack          NVARCHAR(30),
    IsDropshipLocation     NVARCHAR(30)
)
