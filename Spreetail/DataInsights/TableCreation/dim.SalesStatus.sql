DROP TABLE IF EXISTS dim.SaleStatus;
GO
CREATE TABLE dim.SaleStatus (
  DimSaleStatusID INT IDENTITY(1,1) PRIMARY KEY,
  SalesStatusID INT NOT NULL,
  StatusProgression INT NOT NULL,
  GenericStatus NVARCHAR(30) NOT NULL,
  SpecificStatus NVARCHAR(30) NOT NULL,
  CreatedOn DATETIME NOT NULL
    DEFAULT GETDATE(),
  CreatedBy NVARCHAR(128) NOT NULL
    DEFAULT SUSER_NAME()
);

GO

SET IDENTITY_INSERT dim.SaleStatus ON;
INSERT INTO dim.SaleStatus (
  DimSaleStatusID,
  SalesStatusID,
  StatusProgression,
  GenericStatus,
  SpecificStatus
)
VALUES (
  0, 0, 0, '?Unknown', '?Unknown'
);
SET IDENTITY_INSERT dim.SaleStatus OFF;