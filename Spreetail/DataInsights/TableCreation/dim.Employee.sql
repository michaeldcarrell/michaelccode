DROP TABLE IF EXISTS dim.Employee;
CREATE TABLE dim.Employee (
  DimEmployeeID INT IDENTITY(1,1) PRIMARY KEY,
  UserID INT NOT NULL,
  UserName NVARCHAR(50) NOT NULL,
  EmployeeID NVARCHAR(20) NOT NULL,
  OrganizationChain VARCHAR(100) NOT NULL,
  FullName NVARCHAR(128) NOT NULL,
  EmailAddress NVARCHAR(255) NOT NULL,
  Department NVARCHAR(100) NOT NULL,
  JobTitle NVARCHAR(100) NOT NULL,
  EmployeeStartDate DATETIME NOT NULL,
  ReportToName NVARCHAR(100) NOT NULL,
  ReportToJobTitle NVARCHAR(100) NOT NULL,
  OrganizationChainNode HIERARCHYID NOT NULL,
  CreatedOn DATETIME NOT NULL
    DEFAULT GETDATE(),
  CreatedBy NVARCHAR(255)
    DEFAULT SUSER_NAME()
);
GO
SET IDENTITY_INSERT dim.Employee ON;
INSERT INTO dim.Employee (
  DimEmployeeID,
  UserID,
  UserName,
  EmployeeID,
  OrganizationChain,
  FullName,
  EmailAddress,
  Department,
  JobTitle,
  EmployeeStartDate,
  ReportToName,
  ReportToJobTitle,
  OrganizationChainNode)
VALUES (
    0,
    0,
    '?Unknown',
    0,
    '0',
    '?Unknown',
    '?Unknown',
    '?Unknown',
    '?Unknown',
    0,
    '?Unknown',
    '?Unknown',
    HIERARCHYID::GetRoot()
);
SET IDENTITY_INSERT dim.Employee OFF;

DROP TABLE IF EXISTS etl.Employee;
CREATE TABLE etl.Employee (
    DimEmployeeID INT,
    UserID INT,
    UserName NVARCHAR(50),
    EmployeeID NVARCHAR(20),
    OrganizationChain VARCHAR(100),
    FullName NVARCHAR(128),
    EmailAddress NVARCHAR(255),
    Department NVARCHAR(100),
    JobTitle NVARCHAR(100),
    EmployeeStartDate DATETIME,
    ReportToName NVARCHAR(100),
    ReportToJobTitle NVARCHAR(100),
    OrganizationChainNode HIERARCHYID
);