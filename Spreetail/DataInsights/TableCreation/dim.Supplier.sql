DROP TABLE IF EXISTS dim.Supplier;
DROP TABLE IF EXISTS etl.Supplier;

CREATE TABLE dim.Supplier (
  DimSupplierID INT IDENTITY(1,1) PRIMARY KEY,
  SupplierID INT NOT NULL,
  ManufacturerID INT NOT NULL,
  SupplierName NVARCHAR(64) NOT NULL,
  BrandName NVARCHAR(64) NOT NULL,
  SupplierBrand NVARCHAR(128) NOT NULL,
  CreatedOn DATETIME NOT NULL
    DEFAULT GETDATE(),
  CreatedBy NVARCHAR(128) NOT NULL
    DEFAULT SUSER_NAME()
);

CREATE TABLE etl.Supplier (
  DimSupplierID INT,
  SupplierID INT,
  ManufacturerID INT,
  SupplierName NVARCHAR(64),
  BrandName NVARCHAR(64),
  SupplierBrand NVARCHAR(128),
);

GO

SET IDENTITY_INSERT dim.Supplier ON;
INSERT INTO dim.Supplier (
  DimSupplierID,
  SupplierID,
  ManufacturerID,
  SupplierName,
  BrandName,
  SupplierBrand
)
VALUES (0, 0, 0, '?Unknown', '?Unknown', '?Unknown')
SET IDENTITY_INSERT dim.Supplier OFF;