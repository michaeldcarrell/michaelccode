CREATE TABLE lookup.SalesChannels (
  sourceChannel NVARCHAR(20) PRIMARY KEY,
  mapChannel NVARCHAR(20) DEFAULT
);