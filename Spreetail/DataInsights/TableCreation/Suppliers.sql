CREATE TABLE etldata.Suppliers (
	SupplierID INT,
	AddressID INT,
	PrimaryAccountManager INT,
	Company NVARCHAR(100),
	LeadTime INT,
  ChangeOperation NCHAR(1),
	SourceTableProcessExecutionID BIGINT
);

CREATE TABLE stage.Suppliers (
	SupplierID INT,
	AddressID INT,
	PrimaryAccountManager INT,
	Company NVARCHAR(100),
	LeadTime INT,
  IsIncrementalInsertOverlap BIT,
	IsSourceSystemActive BIT,
	SourceTableProcessExecutionID BIGINT
);