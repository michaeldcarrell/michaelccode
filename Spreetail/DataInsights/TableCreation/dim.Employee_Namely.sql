DROP TABLE IF EXISTS dim.Employee;
CREATE TABLE dim.Employee (
  DimEmployeeID INT IDENTITY(1,1) PRIMARY KEY,
  EmployeeID NVARCHAR(30) NOT NULL,
  OrgChain VARCHAR(50) NOT NULL,
  FullName NVARCHAR(128) NOT NULL,
  Department NVARCHAR(128) NOT NULL,
  JobTitle NVARCHAR(255) NOT NULL,
  ReportsToName NVARCHAR(128) NOT NULL,
  ReportsToTitle NVARCHAR(255) NOT NULL,
  OrgChainNode HIERARCHYID,
  CreatedOn DATETIME NOT NULL
    DEFAULT GETDATE(),
  CreatedBy NVARCHAR(255)
    DEFAULT SUSER_NAME()
);
SET IDENTITY_INSERT dim.Employee ON;
INSERT INTO dim.Employee (
  DimEmployeeID,
  OrgChain,
  EmployeeID,
  FullName,
  Department,
  JobTitle,
  ReportsToName,
  ReportsToTitle
)
VALUES (
  0,
  '?Unknown',
  '?Unknown',
  '?Unknown',
  '?Unknown',
  '?Unknown',
  '?Unknown',
  '?Unknown'
);

DROP TABLE IF EXISTS etl.Employee;
CREATE TABLE etl.Employee (
  DimEmployeeID INT NOT NULL,
  EmployeeID NVARCHAR(30) NOT NULL,
  OrgChain VARCHAR(50) NOT NULL,
  FullName NVARCHAR(128) NOT NULL,
  Department NVARCHAR(128) NOT NULL,
  JobTitle NVARCHAR(255) NOT NULL,
  ReportsToName NVARCHAR(128) NOT NULL,
  ReportsToTitle NVARCHAR(255) NOT NULL,
  OrgChainNode HIERARCHYID
);