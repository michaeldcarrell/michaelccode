CREATE TABLE etl.Package
(
  PackageID       INT,
  ShipmentID      INT,
  TrackingNumber  NVARCHAR(255),
  CanceledLabel   NVARCHAR(20),
  ShippingService NVARCHAR(50),
  ShippingCo      NVARCHAR(50)
);
