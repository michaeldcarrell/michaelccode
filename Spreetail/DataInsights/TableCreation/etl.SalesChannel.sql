DROP TABLE IF EXISTS etl.SalesChannel;
CREATE TABLE etl.SalesChannel (
  MarketplaceKey NVARCHAR(50),
  SalesChannel NVARCHAR(50),
  PartnerName NVARCHAR(50),
  ChannelType NVARCHAR(50)
)