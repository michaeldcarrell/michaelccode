CREATE TABLE etldata.items_InventoryStatusType (
	InventoryStatusID INT,
	StatusID INT,
	StatusFamily NVARCHAR(100),
  ChangeOperation NCHAR(1),
	SourceTableProcessExecutionID BIGINT
);

CREATE TABLE stage.items_InventoryStatusType (
	InventoryStatusID INT,
	StatusID INT,
	StatusFamily NVARCHAR(100),
  IsIncrementalInsertOverlap BIT,
	IsSourceSystemActive BIT,
	SourceTableProcessExecutionID BIGINT
);