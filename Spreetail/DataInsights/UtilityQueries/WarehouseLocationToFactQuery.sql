SELECT *
FROM dp_Adjustments adj
LEFT OUTER JOIN dp_PackageItems dPI ON adj.PkgItemID = dPI.PkgItemID
LEFT OUTER JOIN dp_Packages dP ON dPI.PackageID = dP.PackageID
LEFT OUTER JOIN dp_Shipments dS ON dP.ShipmentID = dS.ShipmentID
LEFT OUTER JOIN btdata.dbo.Sales s ON s.SaleID = dpi.SaleID