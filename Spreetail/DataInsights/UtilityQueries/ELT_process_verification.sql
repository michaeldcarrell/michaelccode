WITH etlProcess AS (
  SELECT
    st.SourceTableID,
    stpe.SourceTableProcessExecutionID,
    st.SourceDatabase,
    st.SourceSchema,
    st.SourceTableName,
    stpe.ChangeTrackingCurrentVersionForNext,
    stpe.ProcessStart,
    stpe.ProcessEnd,
    CAST(ProcessEnd - ProcessStart AS TIME) RunTimeHMS,
    CASE
      WHEN MAX(stpe.SourceTableProcessExecutionID) OVER (PARTITION BY st.SourceTableID ORDER BY stpe.SourceTableID) =
           stpe.SourceTableProcessExecutionID
        THEN 1
      ELSE 0
      END                                   MostRecent,
    stpe.CompletedSuccessfully,
    stpe.CreatedOn,
    stpe.CreatedBy,
    stpe.ModifiedOn,
    stpe.ModifiedBy
  FROM etl.SourceTable st
         LEFT OUTER JOIN etl.SourceTableProcessExecution stpe ON stpe.SourceTableID = st.SourceTableID
  WHERE 1 = 1
)
SELECT SourceDatabase,
  SourceSchema,
  SourceTableName,
  ChangeTrackingCurrentVersionForNext ChangeTrackingNum,
  RunTimeHMS,
  CompletedSuccessfully,
  ModifiedOn,
  ModifiedBy,
  CreatedOn,
  CreatedBy
FROM etlProcess
WHERE 1=1
AND MostRecent = 1
--AND SourceDatabase = ''
--AND SourceSchema = ''
AND SourceTableName = 'Suppliers'
ORDER BY SourceTableID, MostRecent DESC