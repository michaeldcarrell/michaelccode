create index InventoryItemIDMapping__ItemID
	on stage.InventoryItemIDMapping (ItemID)
go

create index InventoryItemIDMapping__ModifiedItemID
	on stage.InventoryItemIDMapping (ModifyItemID)
go

