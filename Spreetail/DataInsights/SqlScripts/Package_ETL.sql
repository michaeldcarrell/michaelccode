SELECT DISTINCT p.PackageID,
                p.ShipmentID,
                CAST(p.TrackingInfo AS NVARCHAR(255)) TrackingNumber,
                CAST(CASE
                       WHEN pq.PrinterID = 1 THEN 'Label Canceled'
                       ELSE 'Label Not Canceled'
                  END AS NVARCHAR(20)) CanceledLabel,
                sc.ShippingService,
                sc.ShippingCo
FROM etldata.dp_Packages p
LEFT OUTER JOIN stage.dp_PrintQueue pq ON pq.PackageID = p.PackageID
LEFT OUTER JOIN lookup.ShippingCo sc ON sc.SourceShippingCo = p.ShippingCo