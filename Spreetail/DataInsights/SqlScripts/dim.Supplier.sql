EXEC ssis.usp_dim_SupplierSourceQuery
WITH RESULT SETS (
  (
    SupplierID INT,
    ManufacturerID INT,
    SupplierName NVARCHAR(64),
    BrandName NVARCHAR(64),
    SupplierBrand NVARCHAR(128)
  )
)