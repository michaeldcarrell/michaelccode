SELECT
   StatusID                              SaleStatusID,
   StatusOrder                           StatusProgression,
   StatusFamily                          GenericStatus,
   StatusText                            SpecificStatus,
   CONCAT(
      '(',
     StatusID, ', ',
      StatusOrder, ', ',
      '''', StatusFamily, ''', ',
      '''', StatusText, '''), '
    )
 FROM stage.SalesStatus
 ORDER BY SaleStatusID