
IF EXISTS (SELECT name FROM sys.indexes WHERE name = 'Items_ItemID_index')
  DROP INDEX Items_ItemID_index ON stage.Items
GO
CREATE CLUSTERED INDEX Items_ItemID_index ON stage.Items(ItemID);

IF EXISTS (SELECT name FROM sys.indexes WHERE name = 'InventoryItemIDMapping_ItemID')
  DROP INDEX InventoryItemIDMapping_ItemID ON stage.InventoryItemIDMapping
  GO
CREATE NONCLUSTERED INDEX InventoryItemIDMapping_ItemID ON stage.InventoryItemIDMapping(ItemID);

IF EXISTS (SELECT name FROM sys.indexes WHERE name = 'InventoryItemIDMapping_ModifiedItemID')
  DROP INDEX InventoryItemIDMapping_ModifiedItemID ON stage.InventoryItemIDMapping
GO
CREATE NONCLUSTERED INDEX InventoryItemIDMapping_ModifiedItemID ON stage.InventoryItemIDMapping(ModifyItemID);