CREATE OR ALTER PROCEDURE ssis.usp_dim_Employee_Relation

AS

BEGIN

    WITH reports AS (
        SELECT CAST(ReportsTo AS VARCHAR) ReportsTo,
            CAST(NamelyProfileID AS VARCHAR) NamelyProfileID,
            CAST(ROW_NUMBER() OVER (PARTITION BY ReportsTo ORDER BY CASE EmployeeID WHEN 'ST00005' THEN 0 ELSE 1 END) AS VARCHAR) + '/' Report
        FROM stage.NamelyProfiles
    ),
    nonode AS (
    --Select a blank master in order to include all employees regardless of if they report to someone
    SELECT CAST(NULL AS VARCHAR) ReportsTo,
        CAST('00000000-0000-0000-0000-000000' AS VARCHAR) NamelyProfileID,
        HIERARCHYID::GetRoot() Node
    UNION ALL
    --recursively joins employees to the blank master down to base employee
    SELECT CAST(e.ReportsTo AS VARCHAR) ReportsTo,
        CAST(e.NamelyProfileID AS VARCHAR) NamelyProfileID,
        CAST(nonode.Node.ToString()+ reports.Report AS HIERARCHYID) Node
    FROM stage.NamelyProfiles e
    JOIN nonode ON CAST(COALESCE(e.ReportsTo, '00000000-0000-0000-0000-000000') AS VARCHAR) = nonode.NamelyProfileID
    JOIN reports ON CAST(COALESCE(e.NamelyProfileID, '00000000-0000-0000-0000-000000') AS VARCHAR) = reports.NamelyProfileID
    )
    SELECT
        COALESCE(u.UserID, 0) UserID,
        COALESCE(u.UserName, 'No Username') Username,
        COALESCE(np.EmployeeID, 'EmployeeID Unassigned') EmployeeID,
        COALESCE(REPLACE(CASE
            WHEN LEN(nonode.Node.ToString()) = 1 THEN ''
            ELSE SUBSTRING(nonode.Node.ToString(), 2, LEN(nonode.Node.ToString())-2)
        END, '/', '|'), '0') OrgChain,
        CASE
            WHEN nonode.Node.ToString() = '/'
                THEN 'Master Employee'
            WHEN np.FirstName IS NULL AND np.LastName IS NULL THEN 'No Name Provided'
            WHEN np.FirstName IS NULL THEN np.LastName
            WHEN np.LastName IS NULL THEN np.FirstName
            ELSE
                CONCAT(np.FirstName, ' ', np.LastName)
            END                                          FullName,
        COALESCE(np.Email, 'No Email') Email,
        COALESCE(np.Department, 'No Department')         Department,
        COALESCE(np.JobTitle, 'No Job Title')            JobTitle,
        COALESCE(cu.EmployeeStartDate, 0) EmployeeStartDate,
        CASE
            WHEN dr.EmployeeID IS NOT NULL
                THEN CONCAT(dr.FirstName, ' ', dr.LastName)
            ELSE 'No Direct Supervisor'
            END                                          ReportToName,
        COALESCE(dr.JobTitle, 'No Direct Supervisor')    ReportToJobTitle,
        COALESCE(nonode.Node, HIERARCHYID::GetRoot()) OrgChainNode
    FROM stage.Users u
    LEFT OUTER JOIN stage.ClockUsers cu ON cu.UserID = u.UserID
    LEFT OUTER JOIN stage.NamelyProfiles np ON np.EmployeeID = cu.EmployeeID
    LEFT OUTER JOIN nonode ON CAST(np.NamelyProfileID AS VARCHAR) = nonode.NamelyProfileID
    LEFT OUTER JOIN stage.NamelyProfiles dr ON dr.NamelyProfileID = np.ReportsTo
    ORDER BY CASE WHEN np.EmployeeID = 'ST00005' THEN 0 --put Brett first
                WHEN ssis.func_RemoveNumericCharacters(nonode.Node.ToString()) != '//' THEN 2 --Users under Brett
                WHEN nonode.Node IS NULL THEN 3 --Users Not In Namely
                ELSE 2 --Users In Namely not under Brett
             END, --Users not in namely last
             nonode.Node

END