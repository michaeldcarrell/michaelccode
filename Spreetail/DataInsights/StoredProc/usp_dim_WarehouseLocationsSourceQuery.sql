CREATE OR ALTER PROCEDURE ssis.usp_dim_WarehouseLocationSourceQuery

AS

BEGIN

    SELECT CAST(COALESCE(il.LocationID, 0) AS INT) LocationID,
           CAST(COALESCE(il.LocationName, 'No Location Name') AS NVARCHAR(50)) LocationName,
           CAST(COALESCE(
               CASE
                    WHEN il.PackedProduct = 1 THEN 'Is Packed Product'
                    ELSE 'Not Packed Product'
               END, '?Unknown')
            AS NVARCHAR(30)) IsPackedProduct,
           CAST(COALESCE(
               CASE
                    WHEN il.PickAndPackEnabled = 1 THEN 'Is Pick and Pack Location'
                    ELSE 'Not Pick and Pack Location'
               END, '?Unknown')
            AS NVARCHAR(30)) IsPickAndPack,
           CAST(COALESCE(
               CASE
                    WHEN il.DSLocation = 1 THEN 'Is Dropship Location'
                    ELSE 'Not Dropship Location'
               END, '?Unknown')
           AS NVARCHAR(30)) IsDropshipLocation,
           CAST(COALESCE(iw.City, '?Unknown') AS NVARCHAR(50)) WarehouseCity,
           CAST(COALESCE(iw.State, '?Unknown') AS NVARCHAR(50)) WarehouseState,
           CAST(COALESCE(iw.WarehouseName, 'Warehouse Not Named') AS NVARCHAR(50)) WarehouseName
    FROM stage.InventoryLocations il
    LEFT OUTER JOIN stage.Inventory_Warehouses iw ON iw.WarehouseID = il.WarehouseID
    WHERE LocationID != 0

END