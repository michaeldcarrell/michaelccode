CREATE OR ALTER PROCEDURE ssis.usp_Merge_Dim_Product

AS

BEGIN
  UPDATE dim.Product
  SET
      ItemID = etldata.ItemID,
      ParentItemID = etldata.ParentItemID,
      ItemRelationIndex = etldata.ItemRelationIndex,
      ItemTitle = etldata.ItemTitle,
      ItemSubTitle = etldata.ItemSubTitle,
      ListingType = etldata.ListingType,
      Brand = etldata.Brand,
      BasePartNumber = etldata.BasePartNumber,
      Supplier = etldata.Supplier,
      GoogleCategoryLevel1 = etldata.GoogleCategoryLevel1,
      GoogleCategoryLevel2 = etldata.GoogleCategoryLevel2,
      GoogleCategoryLevel3 = etldata.GoogleCategoryLevel3,
      GoogleCategoryLevel4 = etldata.GoogleCategoryLevel4,
      FullGoogleCategoryPath = etldata.FullGoogleCategoryPath,
      MarketplaceAssortment = etldata.MarketplaceAssortment,
      SpreetailAssortment = etldata.SpreetailAssortment,
      PrimaryVendorManager = etldata.PrimaryVendorManager
      FROM etl.Product etldata
      JOIN dim.Product dim ON dim.DimProductID = etldata.DimProductID
END