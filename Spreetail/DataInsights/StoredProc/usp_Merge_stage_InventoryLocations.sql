CREATE PROCEDURE [ssis].[usp_Merge_stage_InventoryLocations]
							@SourceTableProcessExecutionID BIGINT
AS

BEGIN

		SET NOCOUNT ON

	INSERT INTO stage.InventoryLocations (
		LocationID
		, WarehouseID
		, LocationName
		--End Table Columns
		, IsIncrementalInsertOverlap
		, IsSourceSystemActive
		, SourceTableProcessExecutionID
	)

	SELECT etldata.LocationID
			, etldata.WarehouseID
			, etldata.LocationName
		 --End Table Columns
			, CASE
					WHEN etldata.ChangeOperation = 'I' AND stage.LocationID IS NOT NULL THEN 1
					ELSE 0
				END --IsIncrementalInsertOverlap
			, 1 --IsSourceSystemActive
			, @SourceTableProcessExecutionID
	FROM etldata.InventoryLocations etldata
		LEFT OUTER JOIN stage.InventoryLocations stage
			ON (etldata.LocationID=stage.LocationID)
	WHERE etldata.ChangeOperation = 'I'

	UPDATE stage
	SET
		WarehouseID = etldata.WarehouseID
		, LocationName = etldata.LocationName
	--OtherColumn = etldata.OtherColumn
	--End Table Columns
		, SourceTableProcessExecutionID = @SourceTableProcessExecutionID
	FROM stage.InventoryLocations stage
	INNER JOIN etldata.InventoryLocations etldata ON (stage.LocationID = etldata.LocationID)
		AND (etldata.ChangeOperation = 'U')

  UPDATE stage
	SET IsSourceSystemActive=0
  	, SourceTableProcessExecutionID = @SourceTableProcessExecutionID
  FROM stage.InventoryLocations stage
  INNER JOIN etldata.InventoryLocations etldata ON (stage.LocationID = etldata.LocationID)
  	AND (etldata.ChangeOperation = 'D')

  SET NOCOUNT OFF

END


