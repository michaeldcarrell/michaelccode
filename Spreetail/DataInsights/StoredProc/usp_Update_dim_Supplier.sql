CREATE OR ALTER PROCEDURE ssis.usp_Merge_dim_Supplier

AS

BEGIN
  UPDATE dim.Supplier
  SET
      SupplierName = etl.SupplierName,
      BrandName = etl.BrandName,
      SupplierBrand = etl.SupplierBrand
  FROM etl.Supplier etl
  LEFT OUTER JOIN dim.Supplier dim ON dim.DimSupplierID = etl.DimSupplierID
END