CREATE OR ALTER PROCEDURE lookup.update_Lookup_SalesChannel

AS

BEGIN
    --Updated 5/20/19 initial creation and Facebook insertion - Michael C


    UPDATE lookup.SalesChannel
    SET MapChannel =
            CASE
                WHEN SourceChannel = '' THEN 'eBay.com'
                WHEN SourceChannel = 'AMZ' THEN 'Amazon.com'
                WHEN SourceChannel = 'Auction' THEN 'eBay.com'
                WHEN SourceChannel = 'com.Amazon.old' THEN 'Amazon.com'
                WHEN SourceChannel = 'Fixed Price' THEN 'eBay.com'
                WHEN SourceChannel = 'GEForSpreetail' THEN 'Google Express - Spreetail.com'
                WHEN SourceChannel = 'GoogleExpress' THEN 'Google Express'
                WHEN SourceChannel = 'HomeDepot.com' THEN 'HomeDepotDSV'
                WHEN SourceChannel = 'Store Item' THEN 'eBay.com'
                WHEN SourceChannel = 'VMI' THEN 'VMInnovations'
                WHEN SourceChannel = 'VMI.Facebook.com' THEN 'Facebook - VMInnovations'
                ELSE MapChannel
            END,
        PartnerName =
            CASE
                WHEN SourceChannel = '' THEN 'eBay'
                WHEN SourceChannel = 'AMZ' THEN 'Amazon'
                WHEN SourceChannel = 'Auction' THEN 'eBay'
                WHEN SourceChannel = 'com.Amazon.old' THEN 'Amazon'
                WHEN SourceChannel = 'Fixed Price' THEN 'eBay'
                WHEN SourceChannel = 'GEForSpreetail' THEN 'Google Express'
                WHEN SourceChannel = 'GoogleExpress' THEN 'Google Express'
                WHEN SourceChannel = 'HomeDepot.com' THEN 'HomeDepot'
                WHEN SourceChannel = 'Store Item' THEN 'eBay'
                WHEN SourceChannel = 'VMI' THEN 'VMInnovations'
                WHEN SourceChannel = 'StrollerVillage' THEN 'VMInnovations'
                WHEN SourceChannel = 'TahoeGear' THEN 'VMInnovations'
                WHEN SourceChannel = 'Website Sale' THEN 'VMInnovations'
                WHEN SourceChannel = 'VMI.Facebook.com' THEN 'Facebook'
                ELSE PartnerName
            END
END