CREATE OR ALTER PROCEDURE ssis.usp_dim_SupplierSourceQuery

AS

BEGIN
  SELECT DISTINCT
    COALESCE(sup.SupplierID, 0)          SupplierID,
    COALESCE(im.ManufID, 0)              ManufacturerID,
    COALESCE(sup.Company, 'No Supplier') SupplierName,
    COALESCE(im.Name, 'No Brand')        Brand,
    CASE
      WHEN im.ManufID IS NULL
        THEN sup.Company + ' - No Brand'
      WHEN sup.SupplierID IS NULL
        THEN 'No Supplier - ' + im.Name
      ELSE CONCAT(sup.Company, '-', im.Name)
      END                                SupplierBrand
  FROM stage.items_ExtendedProperties iep
         LEFT OUTER JOIN stage.rcv_POItems poi ON poi.ItemID = iep.ItemID
         LEFT OUTER JOIN stage.rcv_POs po ON po.POID = poi.POID
         LEFT OUTER JOIN stage.Suppliers sup ON sup.SupplierID = po.SupplierID
         LEFT OUTER JOIN stage.items_Manufacturers im ON iep.ManufacturerID = im.ManufID
  WHERE (sup.SupplierID IS NOT NULL
  OR im.ManufID IS NOT NULL)
END
