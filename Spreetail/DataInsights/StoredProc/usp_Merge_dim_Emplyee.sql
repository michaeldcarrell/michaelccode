CREATE OR ALTER PROCEDURE ssis.usp_Merge_dim_Employee

AS

BEGIN
  UPDATE dim.Employee
  SET
      UserID = etl.UserID,
      UserName = etl.UserName,
      EmployeeID = etl.EmployeeID,
      OrgChain = etl.OrgChain,
      FullName = etl.OrgChain,
      EmailAddress = etl.EmailAddress,
      Department = etl.Department,
      JobTitle = etl.JobTitle,
      EmployeeStartDate = etl.EmployeeStartDate,
      ReportToName = etl.ReportToName,
      ReportToJobTitle = etl.ReportToJobTitle,
      OrgChainNode = etl.OrgChainNode
  FROM etl.Employee etl
  LEFT OUTER JOIN dim.Employee dim ON dim.DimEmployeeID = etl.DimEmployeeID
END