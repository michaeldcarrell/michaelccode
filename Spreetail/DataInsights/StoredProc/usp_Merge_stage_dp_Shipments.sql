CREATE PROCEDURE [ssis].[usp_Merge_stage_dp_Shipments]
@SourceTableProcessExecutionID BIGINT
AS 

BEGIN 

SET NOCOUNT ON 

INSERT INTO stage.dp_Shipments (
ShipmentID
, BatchID
, ShipmentDate
, ReadyToPrint
, Completed
, Insured
, InsuranceValue
, Signature
, ShippingAddressID
, IsInternational
, TotalShipping
, ShipmentValue
, AdjGuid
, PrintType
, OriginID
, IsRapidFire
, ProcessedUserID
--End Table Columns 
, IsIncrementalInsertOverlap 
, IsSourceSystemActive 
, SourceTableProcessExecutionID
) 

SELECT
etldata.ShipmentID
, etldata.BatchID
, etldata.ShipmentDate
, etldata.ReadyToPrint
, etldata.Completed
, etldata.Insured
, etldata.InsuranceValue
, etldata.Signature
, etldata.ShippingAddressID
, etldata.IsInternational
, etldata.TotalShipping
, etldata.ShipmentValue
, etldata.AdjGuid
, etldata.PrintType
, etldata.OriginID
, etldata.IsRapidFire
, etldata.ProcessedUserID
--End Table Columns 
, CASE 
WHEN etldata.ChangeOperation = 'I' AND stage.ShipmentID IS NOT NULL THEN 1 
ELSE 0
END --IsIncrementalInsertOverlap 
, 1 --IsSourceSystemActive 
, @SourceTableProcessExecutionID
FROM etldata.dp_Shipments etldata 
LEFT OUTER JOIN stage.dp_Shipments stage 
ON (etldata.ShipmentID=stage.ShipmentID)
WHERE etldata.ChangeOperation = 'I' 

UPDATE stage 
SET
ShipmentID = etldata.ShipmentID
, BatchID = etldata.BatchID
, ShipmentDate = etldata.ShipmentDate
, ReadyToPrint = etldata.ReadyToPrint
, Completed = etldata.Completed
, Insured = etldata.Insured
, InsuranceValue = etldata.InsuranceValue
, Signature = etldata.Signature
, ShippingAddressID = etldata.ShippingAddressID
, IsInternational = etldata.IsInternational
, TotalShipping = etldata.TotalShipping
, ShipmentValue = etldata.ShipmentValue
, AdjGuid = etldata.AdjGuid
, PrintType = etldata.PrintType
, OriginID = etldata.OriginID
, IsRapidFire = etldata.IsRapidFire
, ProcessedUserID = etldata.ProcessedUserID
--OtherColumn = etldata.OtherColumn 
--End Table Columns 
, SourceTableProcessExecutionID = @SourceTableProcessExecutionID
FROM stage.dp_Shipments stage 
INNER JOIN etldata.dp_Shipments etldata ON (stage.ShipmentID = etldata.ShipmentID) 
AND (etldata.ChangeOperation = 'U')


UPDATE stage 
SET
IsSourceSystemActive=0 
, SourceTableProcessExecutionID = @SourceTableProcessExecutionID
FROM stage.dp_Shipments stage 
INNER JOIN etldata.dp_Shipments etldata ON (stage.ShipmentID = etldata.ShipmentID) 
AND (etldata.ChangeOperation = 'D')

SET NOCOUNT OFF 

END