CREATE OR ALTER PROCEDURE ssis.usp_Merge_dim_SalesChannel

AS

BEGIN

  UPDATE dim.SalesChannel
  SET
      SalesChannel = etl.SalesChannel,
      PartnerName = etl.PartnerName,
      ChannelType = etl.ChannelType
  FROM etl.SalesChannel etl
  LEFT OUTER JOIN dim.SalesChannel dim ON dim.MarketplaceKey = etl.MarketplaceKey
  WHERE etl.MarketplaceKey = dim.MarketplaceKey

END