CREATE OR ALTER PROCEDURE ssis.usp_Merge_Dim_Package

AS

BEGIN
  --Update Query
  UPDATE dim.Package
  SET
      TrackingNumber = petl.TrackingNumber,
      CanceledLabel = petl.CanceledLabel,
      ShippingService = petl.ShippingService,
      ShippingCo = petl.CanceledLabel
  FROM dim.Package pdim
  JOIN etl.Package petl ON (petl.PackageID = pdim.PackageID
                              AND petl.ShipmentID = pdim.ShipmentID)

  --Insert Query
  INSERT INTO dim.Package(
    ShipmentID,
    PackageID,
    TrackingNumber,
    CanceledLabel,
    ShippingService,
    ShippingCo)

  SELECT petl.ShipmentID,
         petl.PackageID,
         petl.TrackingNumber,
         petl.CanceledLabel,
         petl.ShippingService,
         petl.ShippingCo
  FROM etl.Package petl
  LEFT JOIN dim.Package pdim ON (petl.PackageID = pdim.PackageID
                              AND petl.ShipmentID = pdim.ShipmentID)
  WHERE pdim.PackageID IS NULL AND pdim.ShipmentID IS NULL

END
go

