CREATE OR ALTER PROCEDURE ssis.usp_dim_EmployeeSourceQuery

AS 
  
BEGIN 
  SELECT COALESCE(UserID, 0) UserID,
  COALESCE(UserName, 'No UserName') UserName,
  COALESCE(EmailAddress, 'No EmailAddress') EmailAddress,
  CASE
    WHEN FirstName IS NULL THEN LastName
    WHEN LastName IS NULL THEN FirstName
    WHEN FirstName IS NULL
        AND LastName IS NULL
      THEN 'No Name Provided'
    ELSE
      CONCAT(FirstName, ' ', LastName)
  END FullName
FROM stage.Users
END