CREATE OR ALTER PROCEDURE lookup.usp_lookup_ShippingCo

AS

BEGIN
  INSERT INTO lookup.ShippingCo (SourceShippingCo, ShippingService, ShippingCo)

  SELECT DISTINCT
    p.ShippingCo SourceShippingCo,
    UPPER(p.ShippingCo) ShippingService,
    UPPER(p.ShippingCo) ShippingCo
  FROM stage.dp_Packages p
  LEFT OUTER JOIN lookup.ShippingCo lsc ON lsc.SourceShippingCo = p.ShippingCo
  WHERE lsc.ShippingCo IS NULL
END