CREATE OR ALTER PROCEDURE ssis.manage_dim_Product
    @Truncate AS NVARCHAR(20),
    @Create AS NVARCHAR(20)

AS

BEGIN

IF @Create = 'CreateDim' OR @Create = 'CreateBoth'
    BEGIN
        DROP TABLE IF EXISTS dim.Product;
        PRINT('Dimension Dropped')
        CREATE TABLE dim.Product (
          DimProductID INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
          ItemID INT NOT NULL,
          ListingType NVARCHAR(20) NOT NULL,
          ItemTitle NVARCHAR(255) NOT NULL,
          ItemSubtitle NVARCHAR(255) NOT NULL,
          Brand NVARCHAR(255) NOT NULL,
          Supplier NVARCHAR(100) NOT NULL,
          PartNumber NVARCHAR(100) NOT NULL,
          GoogleCategoryLevel1 NVARCHAR(60) NOT NULL,
          GoogleCategoryLevel2 NVARCHAR(60) NOT NULL,
          GoogleCategoryLevel3 NVARCHAR(60) NOT NULL,
          GoogleCategoryLevel4 NVARCHAR(60) NOT NULL,
          FullGoogleCategoryPath NVARCHAR(225) NOT NULL,
          MarketplaceAssortment NVARCHAR(25) NOT NULL,
          SpreetailRoomOfHouse NVARCHAR(25) NOT NULL,
          SpreetailAssortment NVARCHAR(25) NOT NULL,
          PrimaryVendorManager NVARCHAR(128) NOT NULL,
          IsListable NVARCHAR(16) NOT NULL,
          CanBePicked NVARCHAR(16) NOT NULL,
          CreatedOn DATETIME NOT NULL
            DEFAULT GETDATE(),
          CreatedBy NVARCHAR(255) NOT NULL
            DEFAULT SUSER_NAME()
        )
        PRINT('Dimension Created')
    END

IF @Truncate = 'TruncateDim' OR @Create = 'CreateBoth' OR @Create = 'CreateDim' OR @Truncate = 'TruncateBoth'
    BEGIN
        TRUNCATE TABLE dim.Product;
        PRINT('Dimension Truncated')
        SET IDENTITY_INSERT dim.Product ON;
        INSERT INTO dim.Product (
          DimProductID,
          ItemID,
          ListingType,
          ItemTitle,
          ItemSubtitle,
          Brand,
          Supplier,
          PartNumber,
          GoogleCategoryLevel1,
          GoogleCategoryLevel2,
          GoogleCategoryLevel3,
          GoogleCategoryLevel4,
          FullGoogleCategoryPath,
          MarketplaceAssortment,
          SpreetailRoomOfHouse,
          SpreetailAssortment,
          PrimaryVendorManager,
          IsListable,
          CanBePicked
        )
        VALUES (
          0, 0, '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown',
          '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown', '?Unknown'
        )
        SET IDENTITY_INSERT dim.Product OFF;
        PRINT('Unknown Record Added')
    END

IF @Create = 'CreateETL' OR @Create = 'CreateBoth'
    BEGIN
        DROP TABLE IF EXISTS etl.Product;
        PRINT('ETL Table Dropped')
        CREATE TABLE etl.Product (
          DimProductID INT,
          ItemID INT,
          ListingType NVARCHAR(20),
          ItemTitle NVARCHAR(255),
          ItemSubtitle NVARCHAR(255),
          Brand NVARCHAR(255),
          Supplier NVARCHAR(100),
          PartNumber NVARCHAR(100),
          GoogleCategoryLevel1 NVARCHAR(60),
          GoogleCategoryLevel2 NVARCHAR(60),
          GoogleCategoryLevel3 NVARCHAR(60),
          GoogleCategoryLevel4 NVARCHAR(60),
          FullGoogleCategoryPath NVARCHAR(225),
          MarketplaceAssortment NVARCHAR(25),
          SpreetailRoomOfHouse NVARCHAR(25),
          SpreetailAssortment NVARCHAR(25),
          PrimaryVendorManager NVARCHAR(128),
          IsListable NVARCHAR(16),
          CanBePicked NVARCHAR(16),
        );
        PRINT('ETL Table Created')
    END

IF @Truncate = 'TruncateETL' OR @Truncate = 'TruncateBoth'
    BEGIN
        TRUNCATE TABLE etl.Product
        PRINT('ETL Table Truncated')
    END

END