import pandas as pd
import numpy as np
import networkx as nx
from pyvis.network import Network
weight = [1, 2, 3]
cost = [5, 4, 3]
a = ['A', 'B', 'C']
b = ['D', 'A', 'E']
df = pd.DataFrame(weight, columns=['weight'])
df[0] = a
df['b'] = b
df['cost'] = cost
G = nx.from_pandas_edgelist(df, 0, 'b', ['weight', 'cost'])

print(df)

graph = Network(width="100%", height="100%")
graph.from_nx(G)

graph.show_buttons(filter_=['physics'])
graph.show('test.html')