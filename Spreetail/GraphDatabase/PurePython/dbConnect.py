import pyodbc as odbc
import pandas as pd


def spreequery(server, database, query):
    conn = odbc.connect('Driver={SQL Server};'
                        'Server=' + server + ';'
                        'Database=' + database + ';'
                        'Trusted_Connection=yes')

    return pd.read_sql(query, conn)


def getitemsrelationship(number):
    if number == -1:
        limiter = ''
    else:
        limiter = f'TOP {number}'
    sql = f"""
    SELECT {limiter}
        sale1.ItemID Item1,
        item1.Title Title1,
        sale2.ItemID Item2,
        item2.Title Title2,
        CONCAT(sale1.ItemID, '-', sale2.ItemID) CompositeRelation,
        COUNT(sale1.SaleID) SaleCount
    FROM stage.dp_PackageItems sale1
    JOIN stage.dp_PackageItems sale2 ON sale2.SaleID = sale1.SaleID AND
                                        sale2.ItemID != sale1.ItemID --Remove single item single sale
    LEFT OUTER JOIN stage.Items item1 ON item1.ItemID = sale1.ItemID
    LEFT OUTER JOIN stage.Items item2 ON item2.ItemID = sale2.ItemID
    LEFT OUTER JOIN (
        SELECT CONCAT(ItemID, '-', ModifyItemID) CompositeRelation FROM stage.InventoryItemIDMapping
        UNION
        SELECT CONCAT(ModifyItemID, '-', ItemID) CompositeRelation FROM stage.InventoryItemIDMapping
    ) existingrelation ON existingrelation.CompositeRelation = CONCAT(sale1.ItemID, '-', sale2.ItemID)
    -- ^ collects all existing bundles
    WHERE 1 = 1
    AND existingrelation.CompositeRelation IS NULL --Removes any bundles that already exist
    GROUP BY item1.Title, sale1.ItemID, sale2.ItemID, item2.Title
    """

    return spreequery('azprod-bi-sql.lapkosoft.local', 'SpreeTL_Dev', sql)