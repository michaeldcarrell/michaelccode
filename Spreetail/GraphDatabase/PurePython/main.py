import dbConnect
import networkx as nx
import pandas as pd
from pyvis.network import Network

itemsRelationship = dbConnect.getitemsrelationship(-1)
item1 = itemsRelationship.loc[:, ['Item1', 'Title1']]
item1.columns = ['ItemID', 'Title']
item2 = itemsRelationship.loc[:, ['Item2', 'Title2']]
item2.columns = ['ItemID', 'Title']


allItems = item1.append(item2, sort=False)
allItems = allItems.drop_duplicates()
print(allItems['ItemID'].to_list())
print(allItems['Title'].to_list())


# nxg = nx.from_pandas_edgelist(itemsRelationship, 'Item1', 'Item2', ['Title1'])

# nx.set_node_attributes(nxg, 'ItemTitle', )


graph = Network(width="65%", height="100%")
# graph.from_nx(nxg)

graph.add_nodes(allItems['ItemID'].to_list(), title=allItems['Title'].to_list())

for index, relation in itemsRelationship.iterrows():
    graph.add_edge(relation['Item1'], relation['Item2'], value=relation['SaleCount'])
    print(relation)

graph.show_buttons(filter_=['physics'])
graph.show('test.html')
