from neo4jrestclient.client import GraphDatabase
import pyodbc as odbc
import pandas as pd
from neo4jrestclient import client


db = GraphDatabase('http://localhost:11015', username='neo4j', password='3dorsdown')

# alice.relationships.create("KNOWS", bob, since=1980)


def createnode(label, itemid, title):
    node = db.nodes.create(itemid=itemid, title=title)
    db.labels.create(label).add(node)


def createrelationship(label, name1, relationship, name2, attr):
    node1id = db.query(f'MATCH (n:{label}) WHERE n.itemid = {name1} RETURN ID(n)', data_contents=True).rows
    node2id = db.query(f'MATCH (n:{label}) WHERE n.itemid = {name2} RETURN ID(n)', data_contents=True).rows
    node1 = db.nodes[node1id[0][0]]
    node2 = db.nodes[node2id[0][0]]
    node1.relationships.create(relationship, node2, since=attr)


# print(db.query('MATCH (n) RETURN distinct labels(n)', data_contents=True).rows)
# print(db.query('MATCH (n) WHERE EXISTS(n.name) RETURN DISTINCT n.name AS name', data_contents=True).graph)

def spreequery(server, database, query):
    conn = odbc.connect('Driver={SQL Server};'
                        'Server=' + server + ';'
                        'Database=' + database + ';'
                        'Trusted_Connection=yes')

    return pd.read_sql(query, conn)


def getitemsrelationship(number):
    sql = f"""
    SELECT TOP {number}
        sale1.SaleID,
        sale1.ItemID Item1,
        item1.Title Title1,
        sale2.ItemID Item2,
        item2.Title Title2
    FROM stage.dp_PackageItems sale1
    JOIN stage.dp_PackageItems sale2 ON sale2.SaleID = sale1.SaleID AND
                                        sale2.ItemID != sale1.ItemID
    LEFT OUTER JOIN stage.Items item1 ON item1.ItemID = sale1.ItemID
    LEFT OUTER JOIN stage.Items item2 ON item2.ItemID = sale2.ItemID
    WHERE 1 = 1
    """

    return spreequery('azprod-bi-sql.lapkosoft.local', 'SpreeTL_Dev', sql)