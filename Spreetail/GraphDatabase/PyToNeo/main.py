import dbConnect

# To Do
# Detect relationship that already exists (SaleID with products)
# Turn lists back to dicts so they are searchable
# Add kwargs in for unlimited number of attributes?

# reset database for testing
"""
detach = False

if detach:
    dbConnect.db.query('MATCH(n) DETACH DELETE n')

    People = [['Alice', 30], ['Bob', 30]]

    for person in People:
        print(person[0] + ' ' + str(person[1]))
        dbConnect.createnode("People", person[0], person[1])


People = [['Alice', 30], ['Newbie', 25], ['Newerbie', 80], ['Bob', 30]]

dbPeople = dbConnect.db.query("MATCH (p:People) RETURN p.name", data_contents=True).rows
oldPeople = []
for person in dbPeople:
    oldPeople.append(person[0])

# define new people
newPeople = []
for newPerson in People:
    personExists = False
    for oldPerson in oldPeople:
        if newPerson[0] == oldPerson:
            personExists = True
    if not personExists:
        newPeople.append(newPerson)

print(newPeople)
for person in newPeople:
    print(person[0] + ' ' + str(person[1]))
    dbConnect.createnode('People', person[0], person[1])

Relationships = [
    ['Bob', 'KNOWS', 'Alice', 1994],
    ['Alice', 'KNOWS', 'Bob', 1994],
    ['Bob', 'KNOWS', 'Newbie', 2005],
    ['Newbie', 'KNOWS', 'Newerbie', 2019],
    ['Bob', 'KNOWS', 'Newerbie', 2019]
]
for relation in Relationships:
    print(relation[0] + relation[1] + relation[2])
    dbConnect.createrelationship(relation[0], relation[1], relation[2], relation[3])

"""

detach = True

if detach:
    dbConnect.db.query('MATCH(n) DETACH DELETE n')

itemsRelationship = dbConnect.getitemsrelationship(100000)
item1 = itemsRelationship.loc[:, ['Item1', 'Title1']]
item1.columns = ['ItemID', 'Title']
item2 = itemsRelationship.loc[:, ['Item2', 'Title2']]
item2.columns = ['ItemID', 'Title']


allItems = item1.append(item2, sort=False)
allItems = allItems.drop_duplicates()

newItems = allItems
for index, item in newItems.iterrows():
    print('ItemID: ' + str(item['ItemID']) + ' Title: ' + str(item['Title']))
    dbConnect.createnode('Item', item['ItemID'], item['Title'])


for index, relation in itemsRelationship.iterrows():
    print('ItemID: ' + str(relation['Item1']) +
          ' SOLD_WITH: ' + str(relation['Item2']) +
          ' IN: ' + str(relation['SaleID']))
    dbConnect.createrelationship('Item', relation['Item1'], 'SOLD_WITH', relation['Item2'], relation['SaleID'])