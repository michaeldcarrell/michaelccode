import pyodbc as odbc
import pandas as pd


def server_connect(server, database):
    connection = odbc.connect('Driver={SQL Server};'
                              f'Server={server}.lapkosoft.local;'
                              f'Database={database};'
                              'Trusted_Connection=yes')

    return connection


def query(connection, sql):
    return pd.read_sql(sql, connection)


def statement(conneciton, sql):
    cursor = conneciton.cursor()
    cursor.execute(sql)
    cursor.commit()
