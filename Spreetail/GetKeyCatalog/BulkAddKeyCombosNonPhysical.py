import pandas as pd
import pyodbc


def exec_proc(key_data):
    sql = f"""
EXEC etl.AddKeyCombo 
    @PK_DatabaseName = '{key_data['PK_Database']}',
    @PK_SchemaName = '{key_data['PK_Schema']}',
    @PK_TableName = '{key_data['PK_Table']}',
    @FK_DatabaseName = '{key_data['FK_Database']}',
    @FK_SchemaName = '{key_data['FK_Schema']}',
    @FK_TableName = '{key_data['FK_Table']}',
    @FK_ColumnName = '{key_data['FK_Column']}',
    @PhysicalKey = 0
    """
    conn = pyodbc.connect('Driver={SQL Server};'
                          f'Server=azprod-bi-sql.lapkosoft.local;'
                          f'Database=SpreeTL_Dev;'
                          'Trusted_Connection=yes')
    cursor = conn.cursor()
    cursor.execute(sql)
    cursor.commit()


all_keys = pd.read_csv('KeyCombos.csv')

for index, key in all_keys.iterrows():
    exec_proc(key)


