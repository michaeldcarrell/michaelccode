import pyodbc
import pandas as pd


def query(server_name, database, sql):
    conn = pyodbc.connect('Driver={SQL Server};'
                          f'Server={server_name}.lapkosoft.local;'
                          f'Database={database};'
                          'Trusted_Connection=yes')
    return pd.read_sql(sql, conn)


def execute_statement(server_name, database, statement):
    conn = pyodbc.connect('Driver={SQL Server};'
                          f'Server={server_name}.lapkosoft.local;'
                          f'Database={database};'
                          'Trusted_Connection=yes')
    cursor = conn.cursor()
    cursor.execute(statement)
    cursor.commit()


def get_current_tables():
    sql = 'SELECT SourceDatabase, SourceSchema, SourceTableName FROM etl.SourceTable'
    tables = query('azprod-bi-sql', 'SpreeTL_Dev', sql)
    return tables


current_tables = get_current_tables()

for index, table in current_tables.iterrows():
    key_set_sql = f"""
    EXEC sp_fkeys @pktable_name = '{table['SourceTableName']}', @pktable_owner = {table['SourceSchema']}
    """
    key_set = query('zoidberg-ro', table['SourceDatabase'], key_set_sql)
    if key_set is not None:
        for row, key in key_set.iterrows():
            azprod_insert_sql = f"""
            EXEC etl.AddKeyCombo @PK_DatabaseName = {key['PKTABLE_QUALIFIER']},
                                 @PK_SchemaName = {key['PKTABLE_OWNER']},
                                 @PK_TableName = {key['PKTABLE_NAME']},
                                 @FK_DatabaseName = {key['FKTABLE_QUALIFIER']},
                                 @FK_SchemaName = {key['FKTABLE_OWNER']},
                                 @FK_TableName = {key['FKTABLE_NAME']},
                                 @FK_ColumnName = {key['FKCOLUMN_NAME']},
                                 @PhysicalKey = 1
            """
            print(azprod_insert_sql)
            execute_statement('azprod-bi-sql', 'SpreeTL_Dev', azprod_insert_sql)

