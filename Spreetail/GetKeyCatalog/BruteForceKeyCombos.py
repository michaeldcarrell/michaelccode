import pandas as pd
import pyodbc


def database_connect(server, conn_database):
    conn = pyodbc.connect('Driver={SQL Server};'
                          f'Server={server}.lapkosoft.local;'
                          f'Database={conn_database};'
                          'Trusted_Connection=yes')
    return conn


def query(conn, query_sql):
    return pd.read_sql(query_sql, conn)


database_pull = f'SELECT name FROM dbo.sysdatabases'

databases = query(database_connect('zoidberg-ro', 'master'), database_pull)

for database in databases['name']:
    column_sql = """
    SELECT TABLE_SCHEMA, TABLE_NAME, COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS
    """
    columns = query(database_connect('zoidberg-ro', database), column_sql)

