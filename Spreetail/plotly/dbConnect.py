import pyodbc as odbc
import pandas as pd


def spreequery(server, database, query):
    conn = odbc.connect('Driver={SQL Server};'
                        'Server=' + server + '.lapkosoft.local;'
                        'Database=' + database + ';'
                        'Trusted_Connection=yes')

    return pd.read_sql(query, conn)


def tltablecolumns(schema, table):
    sql = f"""
    SELECT
        CONCAT('CAST(',
         COLUMN_NAME,
         ' AS ',
         CASE
          WHEN DATA_TYPE LIKE 'text' THEN 'text'
          WHEN DATA_TYPE = 'decimal'
           THEN CONCAT(DATA_TYPE, '(', NUMERIC_PRECISION, ', ', NUMERIC_SCALE, ')')
          WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL
           THEN CONCAT(DATA_TYPE, '(', REPLACE(CHARACTER_MAXIMUM_LENGTH, '-1', 'MAX'), ')')
          ELSE DATA_TYPE
         END,
         ') AS ', COLUMN_NAME
        ) SourceQueryCol,
         CONCAT('CAST(source.',
         COLUMN_NAME,
         ' AS ',
         CASE
          WHEN DATA_TYPE LIKE 'text' THEN 'text'
          WHEN DATA_TYPE = 'decimal'
           THEN CONCAT(DATA_TYPE, '(', NUMERIC_PRECISION, ', ', NUMERIC_SCALE, ')')
          WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL
           THEN CONCAT(DATA_TYPE, '(', REPLACE(CHARACTER_MAXIMUM_LENGTH, '-1', 'MAX'), ')')
          ELSE DATA_TYPE
         END,
         ') AS ', COLUMN_NAME
         ) SourceQueryCol_source
         FROM INFORMATION_SCHEMA.COLUMNS
         WHERE TABLE_SCHEMA = '{schema}'
         AND TABLE_NAME = '{table}'
        AND COLUMN_NAME NOT IN ('IsIncrementalInsertOverlap',
                                'IsSourceSystemActive',
                                'SourceTableProcessExecutionID')
    """

    sourcecolumns = spreequery('azprod-bi-sql', 'SpreeTL_Dev', sql)
    return sourcecolumns


def databases(server):
    sql = f"SELECT name [label], name [value] FROM master.dbo.sysdatabases"
    databasenames = spreequery(server, 'master', sql)
    return databasenames


def schemas(server, database):
    sql = f"SELECT DISTINCT TABLE_SCHEMA [label], TABLE_SCHEMA [value] FROM {database}.INFORMATION_SCHEMA.TABLES"
    schemanames = spreequery(server, database, sql)
    return schemanames


def tables(server, database, schema):
    sql = f"""
        SELECT TABLE_NAME [label], TABLE_NAME [value] 
        FROM {database}.INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '{schema}'
    """
    tablenames = spreequery(server, database, sql)
    return tablenames


def columns(server, database, schema, table):
    sql = f"""
        SELECT COLUMN_NAME [label], COLUMN_NAME [value]
        FROM {database}.INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = '{table}'
        AND TABLE_SCHEMA = '{schema}'
    """
    columnnames = spreequery(server, database, sql)
    return columnnames

