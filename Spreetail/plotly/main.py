import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_auth
import dbConnect

VALID_USERNAME_PASSWORD_PAIRS = [
    ['michael', 'pass']
]

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

databases = dbConnect.databases(server='zoidberg-ro').to_dict('records')

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)
app.layout = html.Div(
    [
        html.Div([
            dcc.Dropdown(
                id='database-dropdown',
                options=databases,
                value='m2',
                placeholder='Select Database'
                ),
            html.Br(),
            dcc.Dropdown(id='schema-dropdown', placeholder='Select Schema',),
            html.Br(),
            dcc.Dropdown(id='table-dropdown', placeholder='Select Table',),
            html.Br(),
            dcc.Dropdown(id='column-dropdown',
                         placeholder='Select Columns',
                         multi=True,),
            html.Hr()
        ], style={'width': '15%', 'display': 'inline-block', 'margin-top': '100px'}),
        html.Div([
            dcc.Tabs(
                id='tabs',
                value='Create Tables',
                children=[
                    dcc.Tab(label='Create Tables', value='tab-tables'),
                    dcc.Tab(label='SSIS Source Query', value='tab-source-query'),
                    dcc.Tab(label='Stored Procedure', value='tab-proc')
                ],
                style={'margin-left': '20px'}
            ),
            html.Div(id='tab-content', style={'height': '90%'})
        ], style={'width': '85%', 'display': 'inline-block', 'float': 'right', 'height': '100%'}),
    ], style={'height': '95vh'}
)


# Set all the dropdown callbacks
@app.callback(
    dash.dependencies.Output('schema-dropdown', 'options'),
    [dash.dependencies.Input('database-dropdown', 'value')]
)
def update_schema_dropdown(database):
    return dbConnect.schemas(server='zoidberg-ro', database=database).to_dict('records')


@app.callback(
    dash.dependencies.Output('table-dropdown', 'options'),
    [dash.dependencies.Input('database-dropdown', 'value'),
     dash.dependencies.Input('schema-dropdown', 'value')]
)
def update_table_dropdown(database, schema):
    return dbConnect.tables(server='zoidberg-ro', database=database, schema=schema).to_dict('record')


@app.callback(
    [dash.dependencies.Output('column-dropdown', 'options'),
     dash.dependencies.Output('column-dropdown', 'value')],
    [dash.dependencies.Input('database-dropdown', 'value'),
     dash.dependencies.Input('schema-dropdown', 'value'),
     dash.dependencies.Input('table-dropdown', 'value')]
)
def update_column_dropdown(database, schema, table):
    columns = dbConnect.columns(server='zoidberg-ro', database=database, schema=schema, table=table)
    return columns.to_dict('record'), columns['value'].tolist()


# Set tab html
@app.callback(
    dash.dependencies.Output('tab-content', 'children'),
    [dash.dependencies.Input('tabs', 'value')]
)
def render_tab_content(tab):
    div_create_tables = html.Div([
            html.Textarea(
                placeholder='Select Database Options to Left',
                style={'width': '100%', 'height': '100%'}
            )
        ], style={'margin-left': '20px', 'margin-top': '10px', 'height': '100%'})
    if tab == 'tab-tables':
        return div_create_tables
    elif tab == 'tab-source-query':
        return html.Div([
            html.H3('Create Source Query')
        ])
    elif tab == 'tab-proc':
        return html.Div([
            html.H3('Create Stored Proc')
        ])
    else:
        return div_create_tables


if __name__ == '__main__':
    app.run_server()
