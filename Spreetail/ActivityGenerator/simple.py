import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    html.H2('Add Activities'),
    dcc.Dropdown(id='activity-owner',
                 placeholder='Select Activity Owner',
                 options=[{'label': 'Hanna', 'value': 'Hanna'}, {'label': 'Michael', 'value': 'Michael'}],
                 value='Hanna'),
    html.Div([dcc.Input(id='activity-name', type='text', placeholder='Activity Name')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Div([dcc.Input(id='activity-weight', type='text', placeholder='Desired Rate 1-10')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Div([dcc.Input(id='activity-temp-min', type='text', placeholder='Minimum Temperature')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Div([dcc.Input(id='activity-temp-max', type='text', placeholder='Maximum Temperature')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Br(),
    html.Div([
        html.Button(id='activity-submit', n_clicks=0, children='Submit')
    ]),
    html.Div(id='submit-confirm', children='')
])


@app.callback(Output('submit-confirm', 'children'),
              [Input('activity-submit', 'n_clicks')],
              [State('activity-owner', 'value'),
               State('activity-name', 'value'),
               State('activity-weight', 'value'),
               State('activity-temp-min', 'value'),
               State('activity-temp-max', 'value')])
def submit_activity(n_clicks, activity_owner, activity_name, activity_weight, activity_temp_min, activity_temp_max):
    return f'Button clicked {n_clicks} times'


if __name__ == '__main__':
    app.run_server(debug=True)