CREATE TABLE History (
	HistoryID INT IDENTITY(1,1) PRIMARY KEY,
	ActivityID INT NOT NULL,
	OwnerID INT NOT NULL,
	CreatedAt DATETIME DEFAULT GETDATE()
)