import pandas as pd
import pyodbc as odbc
import json
import requests
from datetime import datetime
from random import randint


def query(sql):
    conn = odbc.connect('Driver={SQL Server};'
                        'Server=.;'
                        'Database=activity_generator;'
                        'Trusted_Connection=yes')
    return pd.read_sql(sql, conn)


def insert(sql):
    conn = odbc.connect('Driver={SQL Server};'
                        'Server=.;'
                        'Database=activity_generator;'
                        'Trusted_Connection=yes')
    cursor = conn.cursor()
    cursor.execute(sql)
    cursor.commit()


def create_activity(activity, owner, weight, temp_min, temp_max, time, daylight, good_weather):
    # add in new fields (Requires daylight, requires good weathers)
    if owner is not None and weight is not None:
        owners = query('SELECT OwnerID, OwnerName FROM Owners')
        owner_id = owners[owners['OwnerName'] == owner]['OwnerID'].tolist()[0]
        sql = f"""
        INSERT INTO Activities (ActivityOwnerID,
            ActivityName,
            ActivityWeight,
            TempMin,
            TempMax,
            EstimatedTimeMinutes,
            RequiresDaylight,
            RequiresGoodWeather)
        VALUES({owner_id},
            '{activity}',
            {weight},
            {temp_min},
            {temp_max},
            {time},
            {daylight},
            {good_weather})
        """
        print(sql)
        insert(sql)


def get_weather(zip_code, testing=False):
    access_token = '2b76cb2e394099e64787e5bceb467510'
    if testing:
        api_base = 'https://samples.openweathermap.org/data/2.5/weather'
    else:
        api_base = 'http://api.openweathermap.org/data/2.5/weather'
    api_options = f'zip={zip_code},us'
    full_call = api_base + '?' + api_options + '&appid=' + access_token
    response = json.loads(requests.get(full_call).content)
    weather = {'conditions': response['weather'][0]['main'],
               'sub_conditions': response['weather'][0]['description'],
               'temp': round((float(response['main']['temp'])-273.15)*(9/5) + 32, 2),
               'sunset': datetime.utcfromtimestamp(response['sys']['sunset']).strftime('%Y-%m-%d %H:%M:%S'),
               'location_id': response['id'],
               'location_name': response['name']}
    sql = f"""
    INSERT INTO WeatherHistory (LocationID, LocationName, ConditionType, ConditionSubType, Temperature)
    VALUES ({weather['location_id']},
        '{weather['location_name']}',
        '{weather['conditions']}', 
        '{weather['sub_conditions']}',
        '{weather['temp']}'
    )
    """
    insert(sql)
    return weather


def get_historical_activity(readability):
    if not readability:
        historical_sql = """
        SELECT
            ActivityHistoryID,
            ActivityID,
            OwnerID
        FROM ActivityHistory
        """
    else:
        historical_sql = """
        SELECT ActivityName Activity,
            OwnerName Owner,
            wh.ConditionType Weather,
            ah.Skipped,
            ah.CreatedAt Date
        FROM ActivityHistory ah
        LEFT OUTER JOIN Activities a ON a.ActivityID = ah.ActivityID
        LEFT OUTER JOIN Owners o ON o.OwnerID = a.ActivityOwnerID
        LEFT OUTER JOIN WeatherHistory wh ON wh.WeatherHistoryID = ah.WeatherHistoryID 
        """

    return query(historical_sql)


def roll_activity(weather_testing=False):
    # Get Historical Data
    activity_history = get_historical_activity(readability=False)

    # Get Weather
    current_weather = get_weather(weather_testing)
    # Calculate owner balancing
    user_balancing = query("""
    With UserCounts AS (
        SELECT OwnerID,
            COUNT(*) ActivityCount
        FROM ActivityHistory
        WHERE Skipped = 0
        GROUP BY OwnerID
    ),
    Total AS (
        SELECT COUNT(*) TotalCount
        FROM ActivityHistory
        WHERE Skipped = 0
    )
    SELECT OwnerID,
        ROUND(1-(CAST(ActivityCount AS FLOAT)/CAST(TotalCount AS FLOAT)), 2) RollProbability
    FROM UserCounts
    JOIN Total ON 1=1
    """)

    # Calculate Activity History Balancing
        # Date Range for all Activities Selected
            # Ratio for balancing activities based on time since last received

    # Select Owner
    # User with more activities chosen have a lower index calculated meaning they have a lower probability of the rand
    # int being lower than their index
    rando = randint(1, 10)/10
    if rando < user_balancing['RollProbability'][0]:
        chosen_owner = user_balancing['OwnerID'][0]
    else:
        chosen_owner = user_balancing['OwnerID'][1]

    # Select Activity
        # Max probability of choice 10 weight with no choices
            #


# roll_activity(weather_testing=True)

get_weather(68510)
