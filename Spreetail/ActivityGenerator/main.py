import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_auth
import dbConnect as dBc

VALID_USERNAME_PASSWORD_PAIRS = [
    ['michael', 'pass'],
    ['hanna', 'pass']
]

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
# Since we're adding callbacks to elements that don't exist in the app.layout,
# Dash will raise an exception to warn us that we might be
# doing something wrong.
# In this case, we're adding the elements through a callback, so we can ignore
# the exception.
app.config.suppress_callback_exceptions = True
auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)

app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])


index_page = html.Div([
    html.H2('Decision Maker', style={'display': 'inline-block'}),
    html.Div([
        html.A(html.Button('Add Activities', id='button'), href='/new-activities')
    ], style={'display': 'inline-block', 'margin-left': '20px'}),
    html.Div([
        html.Button('Roll for Activity', id='activity-roll', n_clicks=0)
    ], style={'display': 'inline-block', 'margin-left': '20px'})
])

page_1_layout = html.Div([
    html.H2(html.U('Add Activities')),
    html.Div([
        html.P(html.U('Activity Owner')),
        dcc.Dropdown(id='activity-owner',
                     placeholder='Select Activity Owner',
                     options=[{'label': 'Hanna', 'value': 'Hanna'}, {'label': 'Michael', 'value': 'Michael'}],
                     value='Hanna')
    ], style={'margin-bottom': '5px', 'width': '110px'}),
    html.Div([html.P(html.U('Activity Owner')),
              dcc.Input(id='activity-name', type='text', placeholder='Activity Name')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Div([html.P(html.U('Desired Weight 1-10')),
              dcc.Input(id='activity-weight', type='text', placeholder='Desired Rate 1-10')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Div([html.P(html.U('Minimum Temperature (F)')),
              dcc.Input(id='activity-temp-min', type='text', placeholder='Minimum Temperature')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Div([html.P(html.U('Maximum Temperature (F)')),
              dcc.Input(id='activity-temp-max', type='text', placeholder='Maximum Temperature')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Div([html.P(html.U('Total Activity Time')),
              dcc.Input(id='activity-time', type='text', placeholder='Estimated Time')],
             style={'display': 'inline-block', 'margin-right': '5px', 'margin-bottom': '5px'}),
    html.Br(),
    html.Div([
        html.P(html.U('Needs Daylight?')),
        dcc.Dropdown(id='activity-daylight',
                     placeholder='Requires Daylight?',
                     options=[{'label': 'True', 'value': 'True'}, {'label': 'False', 'value': 'False'}])
    ], style={'margin-bottom': '5px', 'width': '175px', 'display': 'inline-block'}),
    html.Div([
        html.P(html.U('Needs Good Weather?')),
        dcc.Dropdown(id='activity-good-weather',
                     placeholder='Requires Good Weather?',
                     options=[{'label': 'True', 'value': 'True'}, {'label': 'False', 'value': 'False'}])
    ], style={'margin-bottom': '5px', 'width': '215px', 'display': 'inline-block'}),
    html.Br(),
    html.Div([
        html.Button(id='activity-submit', n_clicks=0, children='Submit Activity')
    ]),
    html.Div(id='submit-confirm', children='')
])


@app.callback(Output('submit-confirm', 'children'),
              [Input('activity-submit', 'n_clicks')],
              [State('activity-owner', 'value'),
               State('activity-name', 'value'),
               State('activity-weight', 'value'),
               State('activity-temp-min', 'value'),
               State('activity-temp-max', 'value'),
               State('activity-time', 'value'),
               State('activity-daylight', 'value'),
               State('activity-good-weather', 'value')])
def submit_activity(n_clicks, activity_owner, activity_name, activity_weight, activity_temp_min, activity_temp_max,
                    activity_time, activity_daylight, activity_good_weather):
    if activity_daylight == 'True':
        activity_daylight = 1
    else:
        activity_daylight = 0

    if activity_good_weather == 'True':
        activity_good_weather = 1
    else:
        activity_good_weather = 0

    if activity_temp_max is None:
        activity_temp_max = 'NULL'
    if activity_temp_min is None:
        activity_temp_min = 'NULL'

    dBc.create_activity(activity_name, activity_owner, activity_weight, activity_temp_min, activity_temp_max,
                        activity_time, activity_daylight, activity_good_weather)
    return f'{activity_name} submitted for {activity_owner}'


# Update the index
@app.callback(dash.dependencies.Output('page-content', 'children'),
              [dash.dependencies.Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/new-activities':
        return page_1_layout
    else:
        return index_page


if __name__ == '__main__':
    app.run_server(debug=True)