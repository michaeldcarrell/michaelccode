﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitchCase
{
    class Program
    {
        static void Main(string[] args)
        {
            var season = Season.Autum;

            switch (season)
            {
                case Season.Autum: //Having two cases next to eachother like this causes cases to act like OR statements
                case Season.Summer:
                    Console.WriteLine("It's perfect to go to the beach.");
                    break;

                default:
                    Console.WriteLine("I don't understand that season!");
                    break;
            }
        }
    }
}
