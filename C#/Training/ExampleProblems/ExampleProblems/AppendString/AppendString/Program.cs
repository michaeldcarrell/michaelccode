﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendString
{
    class Program
    {
        static void Main(string[] args)
        {
            var primaryKey = "key";
            var columns = "column1, column2, column3";
            var testingMode = true;
            var processType = "Incremental Load";
            var selectionStr = "SELECT";
            var changeTrackingCurrentVersionForCurrentLoad = 1254532345;
            var sourceSchema = "dbo";
            var sourceTable = "Items";
            string sourceColumnSec = string.Empty;
            string sourceQuery = string.Empty;

            columns = columns.Replace(" ", string.Empty);

            List<string> columnList = columns.Split(',').ToList();

            foreach (var column in columnList)
            {
                sourceColumnSec += "\t, source.[" + column + "]\n";
            }

            //Needs to incorperate Full Load and Testing mode locig
            if (testingMode && processType == "Full Load")
            {
                selectionStr = "SELECT TOP 100";
            }

            if (processType == "Full Load")
            {
                sourceQuery = selectionStr + "\n\t" + primaryKey + "\n" + sourceColumnSec +
                     "\t,CAST('I' AS NCHAR(1)) AS [ChangeOperation]\nFROM " + 
                     sourceSchema + "." + sourceTable + "source\nWHERE 1 = 1\n";
            }
            else
            {
                sourceQuery = selectionStr + "\n\tsource_changetracking." + primaryKey + "\n" + sourceColumnSec +
                    "\t,CAST(source_changetracking.SYS_CHANGE_OPERATION AS NCHAR(1)) AS [ChangeOperation]" +
                    "FROM CHANGETABLE(CHANGES" + sourceSchema + "." + sourceTable + ", " + changeTrackingCurrentVersionForCurrentLoad + 
                    ") source_changetracking\n\tINNER JOIN " + sourceSchema + "." + sourceTable + " source on (source_changetracking." + 
                    primaryKey + "=source." + primaryKey + ")";

            }

            Console.WriteLine(sourceQuery);
        }
    }
}
