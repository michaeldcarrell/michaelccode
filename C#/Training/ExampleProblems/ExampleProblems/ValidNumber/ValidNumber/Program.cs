﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValidNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input number between 1-10");

            while (true)
            {
                int number = Convert.ToInt32(Console.ReadLine());
                if (number < 1 || number > 10)
                {
                    Console.WriteLine("Invalid Number!\nTry Again");
                }
                else
                {
                    Console.WriteLine("Valid Number Entered!");
                    break;
                }
            }
        }
    }
}
