﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace MaxNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(@"Input two numbers to determine a maximum value:
Number 1: ");

            int numberA;
            int numberB;

            while (!int.TryParse(Console.ReadLine(), out numberA))
            {
                Console.Write("Invalid value entered, Try again:");
            }

            Console.WriteLine("Number 2:");

            while (!int.TryParse(Console.ReadLine(), out numberB))
            {
                Console.Write("Invalid value entered, Try again:");
            }

            if (numberA > numberB)
            {
                Console.WriteLine("The first number is larger.");
            }
            else
            {
                Console.WriteLine("The second number is larger.");
            }
        }
    }
}
