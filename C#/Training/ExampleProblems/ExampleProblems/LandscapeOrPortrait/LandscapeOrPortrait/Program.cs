﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LandscapeOrPortrait
{
    class Program
    {
        static void Main(string[] args)
        {
            int height;
            int width;

            Console.WriteLine("Enter width of object:");

            while (!int.TryParse(Console.ReadLine(), out width))
            {
                Console.WriteLine("Invalid value entered, try again:");
            }

            Console.WriteLine("Enter height of object:");

            while (!int.TryParse(Console.ReadLine(), out height))
            {
                Console.WriteLine("Invalid value entered, try again:");
            }

            if (height > width)
            {
                Console.WriteLine("Object is portrait.");
            }
            else
            {
                Console.WriteLine("Object is landscape.");
            }

        }
    }
}
