﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SetSourceQuery
{
    class Program
    {
        static void Main(string[] args)
        {
            var columns = Console.ReadLine();

            Console.WriteLine(columns);

            columns = columns.Replace(" ", string.Empty);

            List<string> columnList = columns.Split(',').ToList();

            foreach (var column in columnList)
            {
                Console.WriteLine(column);
            }
        }
    }
}
