DROP TABLE IF EXISTS stage.StockItems;
CREATE TABLE stage.StockItems(
  StockItemID INT,
  StockItemName NVARCHAR(100),
  SupplierID INT,
  ColorID INT,
  UnitPackageID INT,
  OuterPackageID INT,
  Brand NVARCHAR(50),
  Size NVARCHAR(50),
  LeadTimeDays INT,
  IsChillerStock BIT,
  UnitPrice DECIMAL(18, 2),
  RecommendedRetailPrice DECIMAL(18, 2),
  Tags NVARCHAR(MAX),
  ValidFrom DATETIME2,
  ValidTo DATETIME2
)