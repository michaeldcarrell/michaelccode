CREATE OR ALTER PROCEDURE etl.usp_Update_Merge_Invoices

AS

BEGIN

	INSERT INTO stage.Invoices (
		InvoiceID,
		CustomerID,
		BillToCustomerID,
		OrderID,
		DeliveryMethodID,
		ContactPersonID,
		AccountsPersonID,
		SalespersonPersonID,
		PackedByPersonID
	)
	SELECT source.InvoiceID,
		source.CustomerID,
		source.BillToCustomerID,
		source.OrderID,
		source.DeliveryMethodID,
		source.ContactPersonID,
		source.AccountsPersonID,
		source.SalespersonPersonID,
		source.PackedByPersonID
	FROM Sales.Invoices source
	LEFT OUTER JOIN stage.Invoices stage ON stage.InvoiceID = source.InvoiceID
	WHERE stage.InvoiceID IS NULL

	UPDATE stage.Invoices
	SET 
		CustomerID = source.CustomerID,
		BillToCustomerID = source.BillToCustomerID,
		OrderID = source.OrderID,
		DeliveryMethodID = source.DeliveryMethodID,
		ContactPersonID = source.ContactPersonID,
		AccountsPersonID = source.AccountsPersonID,
		SalespersonPersonID = source.SalespersonPersonID,
		PackedByPersonID = source.PackedByPersonID
	FROM Sales.Invoices source
	LEFT OUTER JOIN stage.Invoices stage ON stage.InvoiceID = source.InvoiceID
	WHERE stage.InvoiceID IS NOT NULL
	AND (
		stage.CustomerID != source.CustomerID
		OR stage.BillToCustomerID != source.BillToCustomerID
		OR stage.OrderID != source.OrderID
		OR stage.DeliveryMethodID != source.DeliveryMethodID
		OR stage.ContactPersonID != source.ContactPersonID
		OR stage.AccountsPersonID != source.AccountsPersonID
		OR stage.SalespersonPersonID != source.SalespersonPersonID
		OR stage.PackedByPersonID != source.PackedByPersonID
	)

END