DROP TABLE IF EXISTS stage.Invoices;
CREATE TABLE stage.Invoices (
	InvoiceID INT,
	CustomerID INT,
	BillToCustomerID INT,
	OrderID INT,
	DeliveryMethodID INT,
	ContactPersonID INT,
	AccountsPersonID INT,
	SalespersonPersonID INT,
	PackedByPersonID INT
)