CREATE TABLE stage.Customers (
  CustomerID INT,
  CustomerName NVARCHAR(100),
  DeliveryCityID INT,
  PostalCityID INT,
  DeliveryAddressLine1 NVARCHAR(60),
  DeliveryAddressLine2 NVARCHAR(60),
  DeliveryPostalCode NVARCHAR(10),
  DeliveryLocation GEOGRAPHY,
  ValidFrom DATETIME2,
  ValidTo DATETIME2
)