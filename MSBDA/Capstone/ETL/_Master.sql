CREATE OR ALTER PROCEDURE etl._Master

AS

BEGIN
	PRINT('Customers')
	EXEC etl.usp_Update_Merge_Customers;
	PRINT('OrderLines')
	EXEC etl.usp_Update_Merge_OrderLines;
	PRINT('Orders')
	EXEC etl.usp_Update_Merge_Orders;
	PRINT('StockItems')
	EXEC etl.usp_Update_Merge_StockItems;
	PRINT('Invoices')
	EXEC etl.usp_Update_Merge_Invoices;
	PRINT('InvoiceLines')
	EXEC etl.usp_Update_Merge_InvoiceLines;
	PRINT('Load dim.Item')
	EXEC etl.usp_dim_Item_Load
	PRINT('Load dim.Customers')
	EXEC etl.usp_dim_Customer_Load
	PRINT('Load fact.Sales')
	EXEC etl.usp_fact_Sale_Load;
END