CREATE OR ALTER PROCEDURE etl.usp_dim_Item_Load

AS

BEGIN

	INSERT INTO dim.Item (
		StockItemID,
		PackageTypeID,
		StockItemName,
		Brand,
		Size,
		LeadTimeDays,
		UnitPackageID,
		QuantityPerOuter,
		Barcode,
		RecommendedRetailPrice,
		ValidFrom,
		ValidTo
	)
	SELECT stage.StockItemID,
		ol.PackageTypeID,
		stage.StockItemName,
		COALESCE(stage.Brand, 'No Brand') Brand,
		COALESCE(stage.Size, 'No Size') Size,
		stage.LeadTimeDays,
		stage.UnitPackageID,
		stage.QuantityPerOuter,
		COALESCE(stage.Barcode, 'No Barcode'),
		stage.RecommendedRetailPrice,
		stage.ValidFrom,
		stage.ValidTo
	FROM stage.StockItems stage
	LEFT OUTER JOIN (
		SELECT DISTINCT
			StockItemID,
			PackageTypeID
		FROM stage.OrderLines
	) ol ON ol.StockItemID = stage.StockItemID
	LEFT OUTER JOIN dim.Item dim ON dim.StockItemID = stage.StockItemID
	WHERE dim.StockItemID IS NULL


	UPDATE dim.Item
	SET 
		PackageTypeID = ol.PackageTypeID,
		StockItemName = stage.StockItemName,
		Brand = COALESCE(stage.Brand, 'No Brand'),
		Size = COALESCE(stage.Size, 'No Size'),
		LeadTimeDays = stage.LeadTimeDays,
		UnitPackageID = stage.UnitPackageID,
		QuantityPerOuter = stage.QuantityPerOuter,
		Barcode = COALESCE(stage.Barcode, 'No Barcode'),
		RecommendedRetailPrice = stage.RecommendedRetailPrice,
		ValidFrom = stage.ValidFrom,
		ValidTo = stage.ValidTo
	FROM stage.StockItems stage
	LEFT OUTER JOIN stage.OrderLines ol ON ol.StockItemID = stage.StockItemID
	LEFT OUTER JOIN dim.Item dim ON dim.StockItemID = stage.StockItemID
	WHERE dim.StockItemID IS NOT NULL
	AND (
		dim.PackageTypeID != ol.PackageTypeID
		OR dim.StockItemName != stage.StockItemName
		OR dim.Brand != COALESCE(stage.Brand, 'No Brand')
		OR dim.Size != COALESCE(stage.Size, 'No Size')
		OR dim.LeadTimeDays != stage.LeadTimeDays
		OR dim.UnitPackageID != stage.UnitPackageID
		OR dim.QuantityPerOuter != stage.QuantityPerOuter
		OR dim.Barcode != COALESCE(stage.Barcode, 'No Barcode')
		OR dim.RecommendedRetailPrice != stage.RecommendedRetailPrice
		OR dim.ValidFrom != stage.ValidFrom
		OR dim.ValidTo != stage.ValidTo
	)

END