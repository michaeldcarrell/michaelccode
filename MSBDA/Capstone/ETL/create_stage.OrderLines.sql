CREATE TABLE stage.OrderLines (
  OrderLineID INT,
  OrderID INT,
  StockItemID INT,
  Description NVARCHAR(100),
  PackageTypeID INT,
  Quantity INT,
  UnitPrice DECIMAL(18,2),
  TaxRate DECIMAL(18,3),
  PickedQuantity INT,
  PickingCompletedWhen DATETIME2
)