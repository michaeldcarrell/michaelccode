CREATE PROCEDURE etl.usp_Update_Merge_Orders

AS

BEGIN
  --Insert new records from source
  INSERT INTO stage.Orders
  (OrderID,
   CustomerID,
   SalespersonPersonID,
   PickedByPersonID,
   OrderDate,
   ExpectedDeliveryDate,
   IsUndersupplyBackordered,
   PickingCompletedWhen)

   SELECT source.OrderID,
          source.CustomerID,
          source.SalespersonPersonID,
          source.PickedByPersonID,
          source.OrderDate,
          source.ExpectedDeliveryDate,
          source.IsUndersupplyBackordered,
          source.PickingCompletedWhen
   FROM Sales.Orders source
   LEFT OUTER JOIN stage.Orders stage ON stage.OrderID = source.OrderID
   WHERE stage.OrderID IS NULL

   --Update changed records from source in stage
   UPDATE stage.Orders
   SET
       OrderID = source.OrderID,
       CustomerID = source.CustomerID,
       SalespersonPersonID = source.SalespersonPersonID,
       PickedByPersonID = source.PickedByPersonID,
       OrderDate = source.OrderDate,
       ExpectedDeliveryDate = source.ExpectedDeliveryDate,
       IsUndersupplyBackordered = source.IsUndersupplyBackordered,
       PickingCompletedWhen = source.PickingCompletedWhen
   FROM Sales.Orders source
   JOIN stage.Orders stage ON stage.OrderID = source.OrderID
   WHERE stage.CustomerID != source.CustomerID
   OR stage.SalespersonPersonID != source.SalespersonPersonID
   OR stage.PickedByPersonID != source.PickedByPersonID
   OR stage.OrderDate != source.OrderDate
   OR stage.ExpectedDeliveryDate != source.ExpectedDeliveryDate
   OR stage.IsUndersupplyBackordered != source.IsUndersupplyBackordered
   OR stage.PickingCompletedWhen != source.PickingCompletedWhen
END