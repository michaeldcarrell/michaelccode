CREATE OR ALTER PROCEDURE etl.usp_Update_Merge_OrderLines

AS

BEGIN
  --Insert new records from source
  INSERT INTO stage.OrderLines
  (OrderLineID,
   OrderID,
   StockItemID,
   Description,
   PackageTypeID,
   Quantity,
   UnitPrice,
   TaxRate,
   PickedQuantity,
   PickingCompletedWhen)

   SELECT source.OrderLineID,
          source.OrderID,
          source.StockItemID,
          source.Description,
          source.PackageTypeID,
          source.Quantity,
          source.UnitPrice,
          source.TaxRate,
          source.PickedQuantity,
          source.PickingCompletedWhen
   FROM Sales.OrderLines source
   LEFT OUTER JOIN stage.OrderLines stage ON stage.OrderLineID = source.OrderLineID
   WHERE stage.OrderLineID IS NULL

   --Update changed records from source in stage
   UPDATE stage.OrderLines
   SET
       OrderID = source.OrderID,
       StockItemID = source.StockItemID,
       Description = source.Description,
       PackageTypeID = source.PackageTypeID,
       Quantity = source.Quantity,
       UnitPrice = source.UnitPrice,
       TaxRate = source.TaxRate,
       PickedQuantity = source.PickedQuantity,
       PickingCompletedWhen = source.PickingCompletedWhen
   FROM Sales.OrderLines source
   JOIN stage.OrderLines stage ON stage.OrderLineID = source.OrderLineID
   WHERE source.OrderLineID IS NOT NULL
   AND (
	   stage.OrderID != source.OrderID
	   OR stage.StockItemID != source.StockItemID
	   OR stage.Description != source.Description
	   OR stage.PackageTypeID != source.PackageTypeID
	   OR stage.Quantity != source.Quantity
	   OR stage.UnitPrice != source.UnitPrice
	   OR stage.TaxRate = source.TaxRate
	   OR stage.PickedQuantity != source.PickedQuantity
	   OR stage.PickingCompletedWhen != source.PickingCompletedWhen
   )
END