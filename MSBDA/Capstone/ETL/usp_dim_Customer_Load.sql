CREATE OR ALTER PROCEDURE etl.usp_dim_Customer_Load

AS

BEGIN

	INSERT INTO dim.Customers (
		CustomerID,
		CustomerName,
		DeliveryCityID,
		PostalCityID,
		DeliveryAddressLine1,
		DeliveryAddressLine2,
		DeliveryPostalCode,
		DeliveryLocaitonLatitude,
		DeliveryLocationLongitude,
		ValidFrom,
		ValidTo
	)
	SELECT stage.CustomerID,
	stage.CustomerName,
	stage.DeliveryCityID,
	stage.PostalCityID,
	stage.DeliveryAddressLine1,
	stage.DeliveryAddressLine2,
	stage.DeliveryPostalCode,
	stage.DeliveryLocationLatitude,
	stage.DeliveryLocationLongitude,
	stage.ValidFrom,
	stage.ValidTo
	FROM stage.Customers stage
	LEFT OUTER JOIN dim.Customers dim ON dim.CustomerID = stage.CustomerID
	WHERE dim.CustomerID IS NULL

	UPDATE dim.Customers
	SET 
		CustomerName = stage.CustomerName,
		DeliveryCityID = stage.DeliveryCityID,
		PostalCityID = stage.PostalCityID,
		DeliveryAddressLine1 = stage.DeliveryAddressLine1,
		DeliveryAddressLine2 = stage.DeliveryAddressLine2,
		DeliveryPostalCode = stage.DeliveryPostalCode,
		DeliveryLocaitonLatitude = stage.DeliveryLocationLatitude,
		DeliveryLocationLongitude = stage.DeliveryLocationLongitude,
		ValidFrom = stage.ValidFrom,
		ValidTo = stage.ValidTo
	FROM stage.Customers stage
	LEFT OUTER JOIN dim.Customers dim ON dim.CustomerID = stage.CustomerID
	WHERE dim.CustomerID IS NOT NULL
	AND (
		dim.CustomerName != stage.CustomerName
		OR dim.DeliveryCityID != stage.DeliveryCityID
		OR dim.PostalCityID != stage.PostalCityID
		OR dim.DeliveryAddressLine1 != stage.DeliveryAddressLine1
		OR dim.DeliveryAddressLine2 != stage.DeliveryAddressLine2
		OR dim.DeliveryPostalCode != stage.DeliveryPostalCode
		OR dim.DeliveryLocaitonLatitude != stage.DeliveryLocationLatitude
		OR dim.DeliveryLocationLongitude != stage.DeliveryLocationLongitude
		OR dim.ValidFrom != stage.ValidFrom
		OR dim.ValidTo != stage.ValidTo
	)

END