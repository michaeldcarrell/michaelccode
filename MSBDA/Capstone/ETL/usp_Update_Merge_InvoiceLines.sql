CREATE OR ALTER PROCEDURE etl.usp_Update_Merge_InvoiceLines

AS

BEGIN

	INSERT INTO stage.InvoiceLines (
		InvoiceLineID,
		InvoiceID,
		StockItemID,
		Descrption,
		PackageTypeID,
		Quantity,
		UnitPrice,
		TaxRate,
		TaxAmount,
		LineProfit,
		ExtendedPrice	
	)
	SELECT source.InvoiceLineID,
		source.InvoiceID,
		source.StockItemID,
		source.Description,
		source.PackageTypeID,
		source.Quantity,
		source.UnitPrice,
		source.TaxRate,
		source.TaxAmount,
		source.LineProfit,
		source.ExtendedPrice
	FROM Sales.InvoiceLines source
	LEFT OUTER JOIN stage.InvoiceLines stage ON stage.InvoiceLineID = source.InvoiceLineID
	WHERE stage.InvoiceLineID IS NULL

	UPDATE stage.InvoiceLines
	SET 
		InvoiceID = source.InvoiceID,
		StockItemID = source.StockItemID,
		Descrption = source.Description,
		PackageTypeID = source.PackageTypeID,
		Quantity = source.Quantity,
		UnitPrice = source.UnitPrice,
		TaxRate = source.TaxRate,
		TaxAmount = source.TaxAmount,
		LineProfit = source.LineProfit,
		ExtendedPrice = source.ExtendedPrice
	FROM Sales.InvoiceLines source
	LEFT OUTER JOIN stage.InvoiceLines stage ON stage.InvoiceLineID = source.InvoiceLineID
	WHERE stage.InvoiceLineID IS NOT NULL
	AND (
		stage.InvoiceID != source.InvoiceID
		OR stage.StockItemID != source.StockItemID
		OR stage.Descrption != source.Description
		OR stage.PackageTypeID != source.PackageTypeID
		OR stage.Quantity != source.Quantity
		OR stage.UnitPrice != source.UnitPrice
		OR stage.TaxRate != source.TaxRate
		OR stage.TaxAmount != source.TaxAmount
		OR stage.LineProfit != source.LineProfit
		OR stage.ExtendedPrice != source.ExtendedPrice
	)

END
