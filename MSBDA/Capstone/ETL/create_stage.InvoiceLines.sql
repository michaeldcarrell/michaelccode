DROP TABLE IF EXISTS stage.InvoiceLines;
CREATE TABLE stage.InvoiceLines (
	InvoiceLineID INT,
	InvoiceID INT,
	StockItemID INT,
	Descrption NVARCHAR(100),
	PackageTypeID INT,
	Quantity INT,
	UnitPrice DECIMAL(18,2),
	TaxRate DECIMAL(18,3),
	TaxAmount DECIMAL(18,2),
	LineProfit DECIMAL(18,2),
	ExtendedPrice DECIMAL(18,2)
);