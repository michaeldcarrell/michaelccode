CREATE OR ALTER PROCEDURE etl.usp_fact_Sale_Load

AS

BEGIN

	INSERT INTO fact.Sale (
		CustomerID,
		OrderID,
		InvoiceID,
		InvoiceLineID,
		StockItemID,
		SupplierID,
		UnitPackageID,
		OuterPackageID,
		Quantity,
		SalePrice,
		SaleTaxRate,
		ItemGrossRevenue,
		GrossRevenue,
		ItemCost,
		LineTaxRate,
		LineTaxAmount,
		TotalCostItem,
		TotalCosts,
		ItemNetProfit,
		TotalNetProfit,
		OrderDate
	)
	SELECT i.CustomerID,
		i.OrderID,
		il.InvoiceID,
		il.InvoiceLineID,
		si.StockItemID,
		si.SupplierID,
		si.UnitPackageID,
		si.OuterPackageID,
		il.Quantity,
		il.UnitPrice SalePrice,
		il.TaxRate SaleTaxRate,
		(il.ExtendedPrice + il.LineProfit)/il.Quantity ItemGrossRevenue,
		il.ExtendedPrice + il.LineProfit GrossRevenue,
		il.UnitPrice * il.Quantity ItemCost,
		il.TaxRate LineTaxRate,
		il.TaxAmount LineTaxAmount,
		il.ExtendedPrice/il.Quantity TotalCostItem,
		il.ExtendedPrice TotalCosts,
		il.LineProfit/il.Quantity ItemNetProfit,
		il.LineProfit TotalNetProfit,
		o.OrderDate
	FROM stage.Invoices i
	LEFT OUTER JOIN stage.InvoiceLines il ON il.InvoiceID = i.InvoiceID
	LEFT OUTER JOIN stage.StockItems si ON si.StockItemID = il.StockItemID
	LEFT OUTER JOIN stage.Orders o ON o.OrderID = i.OrderID
	LEFT OUTER JOIN fact.Sale fact ON fact.InvoiceLineID = il.InvoiceLineID
	WHERE fact.InvoiceLineID IS NULL

	UPDATE fact.Sale
	SET
		CustomerID = i.CustomerID,
		OrderID = i.OrderID,
		InvoiceID = il.InvoiceID,
		InvoiceLineID = il.InvoiceLineID,
		StockItemID = si.StockItemID,
		SupplierID = si.SupplierID,
		UnitPackageID = si.UnitPackageID,
		OuterPackageID = si.OuterPackageID,
		Quantity = il.Quantity,
		SalePrice = il.UnitPrice,
		SaleTaxRate = il.TaxRate,
		ItemGrossRevenue = (il.ExtendedPrice + il.LineProfit)/il.Quantity,
		GrossRevenue = il.ExtendedPrice + il.LineProfit,
		ItemCost = il.UnitPrice * il.Quantity,
		LineTaxRate = il.TaxRate,
		LineTaxAmount = il.TaxAmount,
		TotalCostItem = il.ExtendedPrice/il.Quantity,
		TotalCosts = il.ExtendedPrice,
		ItemNetProfit = il.LineProfit/il.Quantity,
		TotalNetProfit = il.LineProfit,
		OrderDate = o.OrderDate
	FROM stage.Invoices i
	LEFT OUTER JOIN stage.InvoiceLines il ON il.InvoiceID = i.InvoiceID
	LEFT OUTER JOIN stage.StockItems si ON si.StockItemID = il.StockItemID
	LEFT OUTER JOIN stage.Orders o ON o.OrderID = i.OrderID
	LEFT OUTER JOIN fact.Sale fact ON fact.InvoiceLineID = il.InvoiceLineID
	WHERE fact.InvoiceLineID IS NOT NULL
	AND (
		fact.CustomerID != i.CustomerID
		OR fact.OrderID != o.OrderID
		OR fact.InvoiceID != i.InvoiceID
		OR fact.InvoiceLineID != il.InvoiceLineID
		OR fact.StockItemID != si.StockItemID
		OR fact.UnitPackageID != si.UnitPackageID
		OR fact.OuterPackageID != si.OuterPackageID
		OR fact.Quantity != il.Quantity
		OR fact.SalePrice != il.UnitPrice
		OR fact.SaleTaxRate != il.TaxRate
		OR fact.LineTaxAmount != il.TaxAmount
		OR fact.TotalCostItem != il.ExtendedPrice/il.Quantity
		OR fact.TotalCosts != il.ExtendedPrice
		OR fact.ItemNetProfit != il.LineProfit/il.Quantity
		OR fact.TotalNetProfit != il.LineProfit
		OR fact.OrderDate != o.OrderDate
	)

END
