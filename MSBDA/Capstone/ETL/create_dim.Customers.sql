DROP TABLE IF EXISTS dim.Customers;
CREATE TABLE dim.Customers (
	DimCustomerID INT IDENTITY(1,1) PRIMARY KEY,
	CustomerID INT NOT NULL,
	CustomerName NVARCHAR(100) NOT NULL,
	DeliveryCityID INT NOT NULL,
	PostalCityID INT NOT NULL,
	DeliveryAddressLine1 NVARCHAR(60) NOT NULL,
	DeliveryAddressLine2 NVARCHAR(60) NOT NULL,
	DeliveryPostalCode NVARCHAR(10) NOT NULL,
	DeliveryLocaitonLatitude FLOAT NOT NULL,
	DeliveryLocationLongitude FLOAT NOT NULL,
	ValidFrom DATETIME2 NOT NULL,
	ValidTo DATETIME2 NOT NULL,
	CreatedOn DATETIME DEFAULT GETDATE(),
	CreatedBy NVARCHAR(128) DEFAULT SUSER_NAME()
);