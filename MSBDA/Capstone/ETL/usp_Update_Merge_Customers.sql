CREATE PROCEDURE etl.usp_Update_Merge_Customers

AS

BEGIN
  --Insert new records from source
  INSERT INTO stage.Customers
  (CustomerID,
   CustomerName,
   DeliveryCityID,
   PostalCityID,
   DeliveryAddressLine1,
   DeliveryAddressLine2,
   DeliveryPostalCode,
   DeliveryLocation,
   ValidFrom,
   ValidTo)

   SELECT source.CustomerID,
          source.CustomerName,
          source.DeliveryCityID,
          source.PostalCityID,
          source.DeliveryAddressLine1,
          source.DeliveryAddressLine2,
          source.DeliveryPostalCode,
          source.DeliveryLocation,
          source.ValidFrom,
          source.ValidTo
   FROM Sales.Customers source
   LEFT OUTER JOIN stage.Customers stage ON stage.CustomerID = source.CustomerID
   WHERE stage.CustomerID IS NULL

   --Update changed records from source in stage
   UPDATE stage.Customers
   SET
       CustomerName = source.CustomerName,
       DeliveryCityID = source.DeliveryCityID,
       PostalCityID = source.PostalCityID,
       DeliveryAddressLine1 = source.DeliveryAddressLine1,
       DeliveryAddressLine2 = source.DeliveryAddressLine2,
       DeliveryPostalCode = source.DeliveryPostalCode,
       DeliveryLocation = source.DeliveryLocation,
       ValidFrom = source.ValidFrom,
       ValidTo = source.ValidTo
   FROM Sales.Customers source
   JOIN stage.Customers stage ON stage.CustomerID = source.CustomerID
   WHERE stage.CustomerName != source.CustomerName
   OR stage.DeliveryCityID != source.DeliveryCityID
   OR stage.PostalCityID != source.PostalCityID
   OR stage.DeliveryAddressLine1 != source.DeliveryAddressLine1
   OR stage.DeliveryAddressLine2 != source.DeliveryAddressLine2
   OR stage.DeliveryPostalCode != source.DeliveryPostalCode
   OR stage.DeliveryLocation.STEquals(source.DeliveryLocation) = 0
   OR stage.ValidFrom != source.ValidFrom
   OR stage.ValidTo != source.ValidTo
END