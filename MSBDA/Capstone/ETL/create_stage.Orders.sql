DROP TABLE IF EXISTS stage.Orders;
CREATE TABLE stage.Orders (
  OrderID INT,
  CustomerID INT,
  SalespersonPersonID INT,
  PickedByPersonID INT,
  OrderDate DATE,
  ExpectedDeliveryDate DATE,
  IsUndersupplyBackordered BIT,
  PickingCompletedWhen DATETIME2
)