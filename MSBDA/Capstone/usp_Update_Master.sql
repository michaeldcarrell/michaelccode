CREATE OR ALTER PROCEDURE etl.usp_Update_Master

AS

BEGIN
  EXEC etl.usp_Update_Merge_Customers
  EXEC etl.usp_Update_Merge_OrderLines
  EXEC etl.usp_Update_Merge_Orders
  EXEC etl.usp_Update_Merge_StockItems
END