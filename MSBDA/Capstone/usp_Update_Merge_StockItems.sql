CREATE PROCEDURE etl.usp_Update_Merge_StockItems

AS

BEGIN
  --Insert new records from source
  INSERT INTO stage.StockItems (
  StockItemID,
  StockItemName,
  SupplierID,
  ColorID,
  UnitPackageID,
  OuterPackageID,
  Brand,
  Size,
  LeadTimeDays,
  IsChillerStock,
  UnitPrice,
  RecommendedRetailPrice,
  Tags,
  ValidFrom,
  ValidTo
)

   SELECT source.StockItemID,
          source.StockItemName,
          source.SupplierID,
          source.ColorID,
          source.UnitPackageID,
          source.OuterPackageID,
          source.Brand,
          source.Size,
          source.LeadTimeDays,
          source.IsChillerStock,
          source.UnitPrice,
          source.RecommendedRetailPrice,
          source.Tags,
          source.ValidFrom,
          source.ValidTo
   FROM Warehouse.StockItems source
   LEFT OUTER JOIN stage.StockItems stage ON stage.StockItemID = source.StockItemID
   WHERE stage.StockItemID IS NULL

   --Update changed records from source in stage
   UPDATE stage.StockItems
   SET
       StockItemName = source.StockItemName,
       SupplierID = source.SupplierID,
       ColorID = source.ColorID,
       UnitPackageID = source.UnitPackageID,
       OuterPackageID = source.OuterPackageID,
       Brand = source.Brand,
       Size = source.Size,
       LeadTimeDays = source.LeadTimeDays,
       IsChillerStock = source.IsChillerStock,
       RecommendedRetailPrice = source.RecommendedRetailPrice,
       Tags = source.Tags,
       ValidFrom = source.ValidFrom,
       ValidTo = source.ValidTo
   FROM Warehouse.StockItems source
   JOIN stage.StockItems stage ON stage.StockItemID = source.StockItemID
   WHERE stage.StockItemName != source.StockItemName
   OR stage.SupplierID != source.SupplierID
   OR stage.ColorID != source.ColorID
   OR stage.UnitPackageID != source.UnitPackageID
   OR stage.OuterPackageID != source.OuterPackageID
   OR stage.Brand != source.Brand
   OR stage.Size != source.Size
   OR stage.LeadTimeDays != source.LeadTimeDays
   OR stage.IsChillerStock != source.IsChillerStock
   OR stage.UnitPrice != source.UnitPrice
   OR stage.RecommendedRetailPrice != source.RecommendedRetailPrice
   OR stage.Tags != source.Tags
   OR stage.ValidFrom != source.ValidFrom
   OR stage.ValidTo != source.ValidTo
END
go

