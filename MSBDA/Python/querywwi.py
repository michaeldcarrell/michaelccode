import pandas as pd
import pyodbc as odbc


class Query:
    @staticmethod
    def wwi(query):
        wwiconnect = odbc.connect('Driver={SQL Server};'
                                  'Server=.;'
                                  'Database=WideWorldImporters;'
                                  'Trusted_Connection=yes;')
        return pd.read_sql(query, wwiconnect)
