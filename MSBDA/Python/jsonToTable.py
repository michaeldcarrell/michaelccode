import pandas as pd
import pyodbc as odbc
import json
from sqlalchemy import create_engine, event
from urllib.parse import quote_plus

pd.set_option('display.width', 10000)
pd.set_option('display.max_columns', 500)

wwi = odbc.connect('Driver={SQL Server};'
                   'Server=.;'
                   'Database=WideWorldImporters;'
                   'Trusted_Connection=yes;')

attributes = pd.read_sql("SELECT StockItemID, CustomFields, Tags FROM Warehouse.StockItems", wwi)

test = pd.DataFrame({'StockItemID': [],
                     'CountryOfManufacture': [],
                     'Tag': []})


# print(attributes)

# countryofmanufacture = pd.read_json(attributes['CustomFields'])
counter = 0
for index, row in attributes.iterrows():
    print(row)
    stockitemid = row['StockItemID']
    countryofmanufacture = json.loads(row['CustomFields'])['CountryOfManufacture']
    tags = json.loads(row['Tags'])
    if len(tags) > 0:
        listlen = len(tags)
    else:
        listlen = 1
        tags = ['']

    lstockitemid = [str(int(stockitemid))] * listlen
    lcountryofmanufacture = countryofmanufacture
    tempframe = pd.DataFrame({'StockItemID': lstockitemid,
                              'CountryOfManufacture': lcountryofmanufacture,
                              'Tag': tags})

    test = test.append(tempframe, ignore_index=True, sort=False)

print(test)

quoted = quote_plus('Driver={SQL Server};' +
                    'Server=.;' +
                    'Database=WideWorldImporters;' +
                    'Trusted_Connection=yes;')
alchemyconn = 'mssql+pyodbc:///?odbc_connect={}'.format(quoted)
engine = create_engine(alchemyconn)


@event.listens_for(engine, 'before_cursor_execute')
def receive_before_cursor_execute(conn, cursor, statement, params, context, executemany):
    print("FUNC call")
    if executemany:
        cursor.fast_executemany = True


table_name = 'Attributes'
test.to_sql(table_name, engine, if_exists='replace', schema='etl', chunksize=1, index_label='ID')

