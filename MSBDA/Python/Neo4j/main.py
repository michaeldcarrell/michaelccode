from neo4jrestclient.client import GraphDatabase
import dbConnect

uri = 'http://localhost:11005'
db = GraphDatabase(uri, username='neo4j', password='3dorsdown')

query = 'MATCH (item:Item) return item.ItemName, item.community'

results = db.query(query)

for r in results:
    dbConnect.writeneo(r)
