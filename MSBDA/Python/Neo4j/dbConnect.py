import pyodbc

wwi = pyodbc.connect('Driver={SQL Server};'
                     'Server=.;'
                     'Database=WideWorldImporters;'
                     'Trusted_Connection=yes')


def writeneo(r):
    cursor = wwi.cursor()
    cursor.execute(
        f'''
        TRUNCATE TABLE etl.Neo4jAlgo;
        INSERT INTO etl.Neo4jAlgo (
            ItemName,
            CommunityNumber
        )
        VALUES ('{str(r[0]).replace("'", "''")}',
                {r[1]});

        EXEC etl.usp_Merge_Neo4jAlgo
        '''
    )
    cursor.commit()
