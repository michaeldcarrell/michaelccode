SELECT a.ActivityID,
	COUNT(h.HistoryID) CountHistory
FROM History h
JOIN Activities a ON a.ActivityID = h.ActivityID
WHERE a.Owner = @DecidedUser
GROUP BY a.ActivityID 
