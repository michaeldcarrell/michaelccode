DROP TABLE IF EXISTS Activities;
CREATE TABLE Activities (
	ActivityID INT IDENTITY(1,1) PRIMARY KEY,
	ActivityName NVARCHAR(64),
	Owner NVARCHAR(128),
	CreatedAt DATETIME
		DEFAULT GETDATE()
);

INSERT INTO dbo.Activities (
	ActivityName,
	Owner
)
VALUES 
	('Hiking', 'Michael'),
	('Biking', 'Michael'),
	('Rock Climbing', 'Michael'),
	('Running', 'Michael'),
	('Badminton', 'Michael'),
	('Racquetball', 'Michael'),
	('Bowling', 'Michael'),
	('Batmiton', 'Hanna'),
	('Walk', 'Hanna'),
	('Winery', 'Hanna'),
	('Paddleboarding', 'Hanna'),
	('Picnic', 'Hanna'),
	('Camping', 'Hanna'),
	('Coffee Date', 'Hanna'),
	('Brewery', 'Hanna'),
	('Bowling', 'Hanna'),
	('New Resturaunt', 'Hanna'),
	('Day Trip Driving', 'Hanna'),
	('Massages', 'Hanna'),
	('Disc Golf', 'Hanna'),
	('Hiking', 'Hanna'),
	('Biking', 'Hanna'),
	('Movies', 'Hanna'),
	('Mani-Pedies', 'Hanna')
