DROP TABLE IF EXISTS dbo.History;
CREATE TABLE dbo.History (
	HistoryID INT IDENTITY(1,1) PRIMARY KEY,
	ActivityID INT,
	LastChosenDate DATETIME
		DEFAULT GETDATE()
);
/*
--Test Results
INSERT INTO History (
	ActivityID
)
VALUES 
	(4),
	(20),
	(21)*/