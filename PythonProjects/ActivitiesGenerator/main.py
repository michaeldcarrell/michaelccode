import pyodbc
import pandas as pd
import random
import getWeather as gw

conn = pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                      'Server=localhost;'
                      'Database=Activities;'
                      'Trusted_Connection=yes;')

activities = pd.read_sql("SELECT * FROM Activities.dbo.Activities", conn)
history = pd.read_sql(open('SQL/Queries/HistoryQuery.sql', 'r').read(), conn)

# ownerRatio > 0.5 more Hanna, < 0.5 more Michael
if not history.empty:
    totalHistoryCount = history.shape[0]
    michaelHistoryCount = history['Owner'].value_counts()['Michael']
    ownerRatio = michaelHistoryCount/totalHistoryCount
else:
    ownerRatio = 0.5

decider = random.randint(0, 100)/100

# if Michael has more values shift distribute to favor Hanna
if decider > ownerRatio:
    decidedOwner = 'Michael'
else:
    decidedOwner = 'Hanna'

historyDistribution = pd.read_sql("DECLARE @DecidedUser NVARCHAR(32)" +
                                  "SET @DecidedUser = '" +
                                  decidedOwner + "'" +
                                  open('SQL/Queries/HistoryDistribution.sql', 'r').read(), conn)

ownerActivities = pd.merge(activities[activities["Owner"] == decidedOwner]
                           , historyDistribution, on="ActivityID", how='outer').fillna(0)


ownerActivities["decisionRolls"] = (max(ownerActivities["CountHistory"]) + 1) - ownerActivities["CountHistory"]

rollList = []

counter = 0
for activity in ownerActivities["decisionRolls"]:
    for roll in range(0, int(ownerActivities.iloc[counter]["decisionRolls"])):
        rollList.append(ownerActivities.iloc[counter]["ActivityID"])
    counter += 1

choice = rollList[random.randint(0, len(rollList) - 1)]

cursor = conn.cursor()
cursor.execute(
    f'''
    INSERT INTO Activities.dbo.History (
        ActivityID
    )
    VALUES ({choice});
    '''
)
conn.commit()

print(activities[activities["ActivityID"] == choice])
