def getweather(zip):
    import requests

    key = 'appid=568ba8f1bd8b0bcdf3a892515a97cf26'
    base = 'http://api.openweathermap.org/data/2.5/weather?'
    options = 'zip=' + str(zip) + ',us'

    call = base + options + '&' + key
    r = requests.get(call)
    conditions = r.json()['weather'][0]["main"]
    current_temp = (float(r.json()["main"]["temp"]) - 273.15)*(9/5) + 32
    min_temp = (float(r.json()["main"]["temp_min"]) - 273.15)*(9/5) + 32
    max_temp = (float(r.json()["main"]["temp_max"]) - 273.15)*(9/5) + 32
    wind_speed = r.json()["wind"]["speed"]
    wind_gust = r.json()["wind"]["gust"]
    sun_rise = r.json()["sys"]["sunrise"]
    sun_set = r.json()["sys"]["sunset"]

    weather = {"conditions": conditions,
               "current_temp": current_temp,
               "max_temp": max_temp,
               "min_temp": min_temp,
               "wind_speed": wind_speed,
               "wind_gust": wind_gust,
               "sun_rise": sun_rise,
               "sun_set": sun_set}

    return weather
