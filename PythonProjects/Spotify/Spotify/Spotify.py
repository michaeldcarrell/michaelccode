def get_spotipy_oauth(self):
        client_id = 'ce333851d4db4ba1b6ccf9eaa52345fc'
        client_secret = '549ec6a308cc4836b7144fc42277a6b2'
        redirect_uri = 'http://localhost:8157/'

        cache_location = os.path.join(
            self.user_cache_dir, 'spotipy_token.cache'
        )
        try:
            # Clean up tokens pre 2.2.1
            # TODO remove soon
            with open(cache_location, 'r') as f:
                contents = f.read()
            data = json.loads(contents)
            if 'scope' in data and data['scope'] is None:
                del data['scope']
                with open(cache_location, 'w') as f:
                    f.write(json.dumps(data))
        except IOError:
            pass
        except ValueError:
            logger.warning(
                'ValueError while getting token info',
                exc_info=True
            )
        return oauth2.SpotifyOAuth(
            client_id, client_secret, redirect_uri,
            scope=SPOFITY_WEB_API_SCOPE,
            cache_path=cache_location
        )

get_spotipy_oauth()